#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        int h,p,a;
        scanf("%d %d %d",&h,&p,&a);
        int f=1;
        if(p>=h)
            printf("%d\n",f);
        else{
            int d=(p-2*a)*(p-2*a)+8*p*(h-a);
            float val=sqrt(d);
            val=(2*a-p+val)/2/p;
            f=val;
            if(fabs(f-val)<=0.00001)
                printf("%d\n",2*f-1);
            else
                printf("%d\n",2*f+1);
        }
    }
    return 0;
}
