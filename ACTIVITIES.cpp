#include<cstdio>
#include<iostream>
#include<vector>
#include<math.h>
using namespace std;
typedef long long ll;
int n;
struct node{
    int s;
    int e;
};
node a[100005];

ll nov(int l,vector<node>& total){
    if(l<n){
        nov(l+1,total);
        //vector<node> x=total;
        vector<node>::iterator it1=total.begin();
        vector<node>::iterator it2=total.end();
        while(it1!=it2){
            if(a[l].e<=it1->s){
                node j;
                j.s=a[l].s;
                j.e=it1->e;
                total.push_back(j);
            }
            else if(it1->e <= a[l].s){
                node j;
                j.s=it1->s;
                j.e=a[l].e;
                total.push_back(j);
            }
            it1++;
        }
    }
    total.push_back(a[l]);
    //cout<<total.size()<<endl;
    return total.size();
}

int main(){
    cin>>n;
    while(n!=-1){
        vector<node> w;
        for(int i=1;i<=n;i++)
            scanf("%d %d",&a[i].s,&a[i].e);
        ll ans1=nov(1,w)%100000000;
        ll ans2=ans1;
        int f=0;
        while(ans1!=0){
            ans1/=10;
            f++;
        }
        f=8-f;
        while(f--)
            cout<<0;
        if(f!=8)
            cout<<ans2;
        cout<<endl;
        cin>>n;
    }
}

