#include<iostream>
#include<stdio.h>
#include<climits>
using namespace std;
int len[1005][1005];
bool visited[1005];
int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        int p,n,m,a,b,c;
        scanf("%d%d%d",&p,&n,&m);
        for(int i=1;i<=n;i++){
            for(int j=1;j<=n;j++)
                len[i][j]=INT_MAX;
            visited[i]=false;
        }
        for(int i=0;i<m;i++){
            scanf("%d%d%d",&a,&b,&c);
            len[a][b]=c;
            len[b][a]=c;
        }
        int f=1,node=1,tl=0,to;
        visited[node]=true;
        while(f<n){
            int min_len=INT_MAX;
            for(int i=1;i<=n;i++){
                if(len[node][i]<min_len && !visited[i]){
                    min_len=len[node][i];
                    to=i;
                }
            }
            //cout<<"node:"<<node<<" to:"<<to<<" min_len:"<<min_len<<endl;
            tl+=min_len;
            visited[to]=true;
            for(int i=1;i<=n;i++)
                len[node][i]=min(len[node][i],len[to][i]);
            f++;
        }
        long long ans=(long long)tl*p;
        printf("%lld\n",ans);
    }
}
