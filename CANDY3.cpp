#include<stdio.h>
int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        long long n,num;
        scanf("%lld",&n);
        long long sum=0;
        for(int i=0;i<n;i++){
            scanf("%lld",&num);
            sum+=(num%n);
        }
        if(sum%n)
            printf("NO\n");
        else
            printf("YES\n");
    }
}
