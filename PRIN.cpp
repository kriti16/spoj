#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<ctime>
using namespace std;
typedef unsigned long long int ull;

#define gc getchar_unlocked
#define pc(x) putchar_unlocked(x);

inline void writeInt (ull n){
    ull N = n, rev, count = 0;
    rev = N;
        if (N == 0) { pc('0'); pc('\n'); return ;}
        while ((rev % 10) == 0) { count++; rev /= 10;} //obtain the count of the number of 0s
        rev = 0;
        while (N != 0) { rev = (rev<<3) + (rev<<1) + N % 10; N /= 10;}  //store reverse of N in rev
        while (rev != 0) { pc(rev % 10 + '0'); rev /= 10;}
        while (count--) pc('0');
    }

void scanint(ull &x)
{
    register ull c = gc();
    x = 0;
    for(;(c<48 || c>57);c = gc());
    for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
}
ull pow(ull a,ull d,ull n){
    if(d==0)
        return 1;
    ull temp=pow(a,d/2,n);
    //cout<<"temp:"<<temp<<endl;
    if(d%2)
        return temp*temp%n*a%n;
    else
        return temp*temp%n;
}

bool mr_test(ull n){
    if(n==1)
        return false;
    if(n==2 || n==3)
        return true;
    if(n&1==0)
        return false;
    int times=1;
    ull a,prev=0;
    while(times--){
        int flag=0;
        ull temp=n-1;
        ull s=0,d;
        while(temp%2==0){
            temp/=2;
            s++;
        }
        d=(n-1)/(1<<s);
        //cout<<"n:"<<n<<" s:"<<s<<" d:"<<d<<endl;
        srand(time(0));
        a=rand()%(n-3)+2;
        while(a==prev)
            a=rand()%(n-3)+2;
        prev=a;
        //cout<<a<<endl;
        ull x=pow(a,d,n);
        //cout<<x<<endl;
        if(x==1 || x==n-1)
            flag=1;
        else{
            for(ull i=1;i<s;i++){
                x=(x*x)%n;
                if(x==n-1){
                    flag=1;
                    break;
                }
                if(x==1)
                    return false;
            }
        }
        if(flag==0)
            return false;
    }
    return true;
}

int main(){
    int t;
    scanf("%d",&t);
    ull m,n;
    while(t--){
        scanint(m);
        scanint(n);
        for(ull i=m;i<=n;i++){

            if(mr_test(i)){
                writeInt(i);
                printf("\n");
            }
        }
    }
}
