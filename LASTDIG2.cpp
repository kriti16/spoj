#include<stdio.h>
#include<cstring>
#include<iostream>
int pow(int n,int p){
    if(p==0)
        return 1;
    return n*pow(n,p-1)%10;
}
using namespace std;
int main(){
    int t;
    scanf("%d",&t);
    scanf("\n");
    while(t--){
        char str1[1005];
        long long b;
        scanf("%s%lld",str1,&b);
        int a=str1[strlen(str1)-1]-'0';
        //cout<<"a:"<<a<<endl;
        if(b==0 && a!=0)
            printf("1\n");
        else{
            int num=(int)(b%4);
            //cout<<"num:"<<num<<endl;
            if(num==0)
                num=4;
            printf("%d\n",pow(a,num));
        }
    }
    return 0;
}
