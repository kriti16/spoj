#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
#include<math.h>
#include<set>
#include<vector>
#include<cstring>
#include<string>
#include<map>
#include<climits>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        int x[3];
        scanf("%d%d%d",&x[0],&x[1],&x[2]);
        char s1[20],s2[20],s3[20],s4[20];
        scanf("%s%s%s%s",s1,s2,s3,s4);
        //printf("%s %s %s %s\n",s1,s2,s3,s4);
        int c;
        if(fabs(s1[0]-s3[0])==1 || fabs(s2[0]-s4[0])==1)
            c=1;
        else if(fabs(s1[0]-s4[0])==1 || fabs(s2[0]-s3[0])==1)
            c=3;
        else
            c=2;
        sort(x,x+3);
        bool first=true;
        if(c==1){
            if(((x[0]^x[1])^x[2]) == 0)
                first=false;
            //printf("xor:%lld\n",x[0]^x[1]^x[2]);
        }
        if(c==2){
            if(x[0]%2==0){
                    if((x[2]==x[0]+1 && x[1]==x[0])|| x[2]==x[0])
                        first=false;
            }
        }
        if(c==3){
            if(x[0]%2)
                first=false;
            else{
                if(x[1]%2){
                    if(x[1]!=(x[0]+1))
                        first=false;
                }
                else{
                    if(x[0]!=x[1])
                        first=false;
                    else if(x[1]==x[2])
                            first=false;
                }
            }
        }
        //cout<<"c:"<<c<<endl;
        if(first){
            if(s1[0]=='A' || s1[0]=='B')
                printf("Aviendha/Birgitte\n");
            else
                printf("Elayne/Nynaeve\n");
        }
        else{
            if(s1[0]=='A' || s1[0]=='B')
                printf("Elayne/Nynaeve\n");
            else
                printf("Aviendha/Birgitte\n");
        }
    }
    return 0;
}

