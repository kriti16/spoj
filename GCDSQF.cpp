#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
#include<math.h>
#include<set>
#include<vector>
#include<cstring>
#include<string>
#include<map>
#include<climits>
using namespace std;
typedef long long ll;
int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        char a[1005],b[1005],gcd[1005];
        scanf("%s%s",a,b);
        int last,flag=0;
        for(int i=0;i<min(strlen(a),strlen(b));i++){
            if(a[i]=='1' && b[i]=='1'){
                gcd[i]='1';
                flag=1;
                last=i;
            }
            else
                gcd[i]='0';
        }
        if(flag){
            for(int i=0;i<=last;i++)
                printf("%c",gcd[i]);
            printf("\n");
        }
        else
            printf("relatively prime\n");
    }
}
