#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<queue>
#include<climits>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;

struct AdjListNode{
    int dest;
    AdjListNode* next;
};
struct AdjList{
    AdjListNode *head;
};
struct Graph{
    int V;
    AdjList* array;
};

struct AdjListNode* newAdjListNode(int dest){
    struct AdjListNode* newNode =
            (struct AdjListNode*) malloc(sizeof(struct AdjListNode));
    newNode->dest = dest;
    newNode->next = NULL;
    return newNode;
}

Graph* createGraph(int V){
    Graph* graph = (Graph*) malloc(sizeof(Graph));
    graph->V = V;
    graph->array = (AdjList*) malloc((V+1) * sizeof(AdjList));
    int i;
    for (i = 1; i <= V; ++i)
        graph->array[i].head = NULL;
    return graph;
}

void addEdge(Graph* graph, int src, int dest){
    AdjListNode* newNode = newAdjListNode(dest);
    newNode->next = graph->array[src].head;
    graph->array[src].head = newNode;
    newNode = newAdjListNode(src);
    newNode->next = graph->array[dest].head;
    graph->array[dest].head = newNode;
}
queue<int> q;
int dist[2005];
void BFS(int node,Graph* graph){
    dist[node]=0;
    q.push(node);
    while(!q.empty()){
        int v=q.front();
        q.pop();
        AdjListNode* pCrawl = graph->array[v].head;
        while(pCrawl){
            if(dist[pCrawl->dest]==INT_MAX){
                dist[pCrawl->dest]=dist[v]+1;
                q.push(pCrawl->dest);
            }
            pCrawl = pCrawl->next;
        }
    }
}

int main(){
    int t;
    scanf("%d",&t);
    for(int i=1;i<=t;i++){
        int V,e,u,v;
        scanf("%d %d",&V,&e);
        Graph* graph = createGraph(V);
        //cout<<hi;
        for(int j=0;j<e;j++){
            scanf("%d%d",&u,&v);
            addEdge(graph,u,v);
        }
        printf("Scenario #%d:\n",i);
        //printGraph(graph);
       for(int j=1;j<=V;j++){
            dist[j]=INT_MAX;
            //cout<<dist[j]<<endl;
        }
        for(int j=1;j<=V;j++){
            if(dist[j]==INT_MAX)
                BFS(j,graph);
        }
        for (v = 1; v <= graph->V; ++v){
            AdjListNode* pCrawl = graph->array[v].head;
            while (pCrawl){
                if(dist[pCrawl->dest]==dist[v]){
                    printf("Suspicious bugs found!\n");
                    break;
                }
                pCrawl = pCrawl->next;
            }
            if(pCrawl)
                break;
        }
        if(v>=V)
            printf("No suspicious bugs found!\n");
    }
    return 0;
}

