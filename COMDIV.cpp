#include<iostream>
#include<stdio.h>
#define gc getchar_unlocked
#define pc(x) putchar_unlocked(x);
    inline void writeInt (long long n)
    {
        long long N = n, rev, count = 0;
        rev = N;
        if (N == 0) { pc('0'); pc('\n'); return ;}
        while ((rev % 10) == 0) { count++; rev /= 10;} //obtain the count of the number of 0s
        rev = 0;
        while (N != 0) { rev = (rev<<3) + (rev<<1) + N % 10; N /= 10;}  //store reverse of N in rev
        while (rev != 0) { pc(rev % 10 + '0'); rev /= 10;}
        while (count--) pc('0');
    }
using namespace std;
void scanint(int &x)
{
    register int c = gc();
    x = 0;
    for(;(c<48 || c>57);c = gc());
    for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
}
using namespace std;
typedef long long ll;
bool num[1000001];
int prime[60000];
int len;

void create_prime(){
    int k=0,j;
    for(int i=2;i<=1000;i++){
        if(num[i]==false){
            prime[k]=i;k++;
            for(j=2;j*i<=1000;j++)
                num[j*i]=true;
        }
    }
    len=k;
}

ll factorise(int n){
    ll total=1;
    for(int i=0;prime[i]*prime[i]<=n && i<len;i++){
            int f=0;
            while(n>0 && n%prime[i]==0){
                f++;
                n/=prime[i];
            }
            total*=(f+1);
            if(n==1)
                break;
        }
        if(n>1)
                total*=2;

        return total;
}

int gcd(int m,int n){
    int a=max(m,n);
    int b=min(m,n);
    int r=a%b;
    while(r!=0){
        a=b;b=r;r=a%b;
    }
    return b;
}

int main(){
    create_prime();
    int t;
    scanint(t);
    while(t--){
        int a,b;
        scanint(a);scanint(b);
        int c=gcd(a,b);
        //cout<<c<<endl;
        writeInt(factorise(c));
        printf("\n");
    }
}
