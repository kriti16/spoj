#include<iostream>
#include<cstdio>
#include<stack>
#include<cstring>
using namespace std;
int main(){
    int n;
    scanf("%d",&n);
    while(n--){
        char s[100000];
        scanf("%s",s);
        stack<char> box;
        box.push(s[0]);
        int len=strlen(s);
        if(s[0]=='}')
            len=0;
        for(int i=1;i<len;i++){
            if(!box.empty()){
                if(box.top()!=s[i])
                    box.pop();
                else
                    box.push(s[i]);
            }
            else{
                box.push(s[i]);
                if(s[i]=='}')
                    break;
            }
        }

        if(box.empty())
            printf(":)\n");
        else
            printf(":(\n");
    }
    return 0;
}
