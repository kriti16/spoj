#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
#include<math.h>
#include<set>
#include<vector>
#include<cstring>
#include<string>
#include<map>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
int main(){
    int n;
    scanf("%d",&n);
    char g1[100005],g2[100005];
    scanf("%s",g1);
    for(int i=0;i<n;i++){
        int f[4]={0,0,0,0};
        f[g1[2*i]-'A']=1;
        f[g1[2*i+1]-'A']=1;
        //printf("%d %d %d %d\n",f[0],f[1],f[2],f[3]);
        for(int j=0;j<4;j++){
            if(f[j]==0){
                g2[2*i]=j+'A';
                f[j]=1;
                break;
            }
        }
        for(int j=0;j<4;j++){
            if(f[j]==0){
                g2[2*i+1]=j+'A';
                f[j]=1;
                break;
            }
        }
        if(i!=0){
            if(g2[2*i]==g2[2*i-1]){
                int temp=g2[2*i];
                g2[2*i]=g2[2*i+1];
                g2[2*i+1]=temp;
            }
        }
    }
    g2[2*n]='\0';
    printf("%s\n",g2);
    return 0;
}

