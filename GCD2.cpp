#include<cstring>
#include<cstdio>
#include<stdio.h>

int gcd(int a, int b)
{
	if (b==0)
		return a;
	else
		return gcd(b,a%b);
}

int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        int a;
        scanf("%d",&a);
        char b[255];
        scanf("%s",b);
        //printf("%s",b);
        int r=0;
        if(a==0)
            printf("%s\n",b);
        else{
        for(int i=0;i<strlen(b);i++)
            r=(10*r+(b[i]-'0'))%a;
        printf("%d\n",gcd(a,r));
        }
    }
}

