#include<cstdio>
#include<math.h>
#include<iostream>
#include<stdlib.h>
#include<string.h>
int n;
typedef long long ll;
ll m;
struct Matrix {
	ll matrix[30][30];
	Matrix() {
		memset(matrix, 0, sizeof(matrix));
	}
}A;

Matrix mul(const Matrix &A, const Matrix &B) {
	Matrix C;
	int i, j, k;
	ll tmp;
	for (i = 0; i < n; i++)
		for (j = 0; j <n ; j++) {
			tmp = 0;
			for (k = 0; k < n; k++) {
				tmp += A.matrix[i][k] * B.matrix[k][j];
			}
			C.matrix[i][j] = tmp % m;
		}
	return C;
}

Matrix pow(const Matrix M, ll p) {
	if (p == 0) {
		Matrix e;
		for (int i = 0; i < n; i++) e.matrix[i][i] = 1;
		return e;
	} else {
		Matrix res = pow(M, p / 2);
		res = mul(res, res);
		if (p % 2) res = mul(res, M);
		return res;
	}
}


int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        int k;
        ll l,ans=0;
        scanf("%d %d %lld",&n,&k,&l);
        m=(ll)(pow(10,k)+0.2);
        //printf("%lld\n",m);
        for(int i=0;i<n;i++)
        {
            if(i==0){
                for(int j=0;j<n;j++)
                    A.matrix[i][j]=1;
            }
            else{
                for(int j=0;j<n;j++){
                    if(j==i-1)
                        A.matrix[i][j]=1;
                    else
                        A.matrix[i][j]=0;
                }
            }
        }
       /* for(int i=0;i<n;i++){
            for(int j=0;j<n;j++)
                printf("%lld ",A.matrix[i][j]);
            printf("\n");
        }  */


        A=pow(A,l-n+1);
        for(int i=0;i<n;i++){
            ans+=(A.matrix[0][i]*(n-1-i))%m;
        }
        printf("%lld\n",ans%m);
    }

}


