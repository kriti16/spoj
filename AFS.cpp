#include<stdio.h>
typedef long long ll;
int fn[1000000]={0};
ll a[1000000];

void create_fn(){
    for(int i=1;i<1000000;i++){
        for(int j=2;i*j<1000000;j++)
                fn[i*j]+=i;
    }
}

void create_val(){
    a[0]=a[1]=0;
    for(int i=2;i<1000000;i++)
        a[i]=a[i-1]+fn[i];
}

int main(){
    create_fn();
    create_val();
    int t;
    scanf("%d",&t);
    int n;
    while(t--){
        scanf("%d",&n);
        printf("%lld\n",a[n]);
    }
    return 0;
}
