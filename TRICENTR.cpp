#include<stdio.h>
#include<cmath>
#include<iostream>
using namespace std;
int main(){
    int t;
    cin>>t;
    while(t--){
        double a,b,c,area,R,D,da,db,dc;
        scanf("%lf%lf%lf%lf",&a,&da,&db,&dc);
        area=3*a*da/2;
        b=a*da/db;
        c=a*da/dc;
        R=a*b*c/4/area;
        D=sqrt(fabs(9*R*R-(a*a+b*b+c*c)));
        D=2*D/3;
        printf("%.3lf %.3lf\n",area,D);
    }
}
