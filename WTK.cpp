#include<iostream>
using namespace std;

int kola(int n,int d){
    if(n==1)
        return 1;
    else{
        int a=(kola(n-1,d+1)+d)%n;
        if(a)
            return a;
        else
            return n;
    }
}

int main(){
    int t,n;
    cin>>t;
    while(t--){
        cin>>n;
        cout<<kola(n,1)<<endl;
    }
    return 0;
}
