#include<iostream>
#include<stdio.h>
using namespace std;
int f[10001]={0};

void sieve(){
    for(int i=1;i<=10000;i++){
        for(int j=1;i*j<=10000;j++)
            f[i*j]++;
    }
}

int main(){
    sieve();
    int n;
    cin>>n;
    long long ans=0;
    for(int i=1;i<=n;i++){
        ans+=((f[i]+1)/2);
    }
    printf("%lld\n",ans);
}
