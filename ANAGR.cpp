#include<stdio.h>
#include<iostream>
#include<string>
#include<ctype.h>
using namespace std;
int main(){
    int t;
    scanf("%d\n",&t);
    while(t--){
        int c[27];
        for(int i=0;i<26;i++){
            c[i]=0;
        }
        string str1,str2;
        getline(cin,str1);
        for(int i=0;i<str1.size();i++){
            if(isupper(str1[i])){
                c[str1[i]-'A']++;
            }
            else if(islower(str1[i]))
                c[str1[i]-'a']++;
        }
        getline(cin,str2);
        for(int i=0;i<str2.size();i++){
            if(isupper(str2[i])){
                c[str2[i]-'A']--;
            }
            else if(islower(str2[i]))
                c[str2[i]-'a']--;
        }
        bool error=false;
        int j=0,flag=0;
        char ans[101];
        int odd=0;
        char odd_c;
        for(int i=0;i<26;i++){
            if(c[i]<0){
                if(flag==2){
                    error=true;
                    break;
                }
                else{
                    flag=1;
                    c[i]=c[i]*(-1);
                    if(c[i]%2){
                        if(odd==0){
                            odd++;
                            odd_c=i+'a';
                            //cout<<"hi"<<endl;
                        }
                        else{
                            error=true;
                            break;
                        }
                    }
                    c[i]/=2;
                    while(c[i]--){
                        ans[j]=i+'a';
                        j++;
                    }
                    //printf("%s\n",ans);
                }
            }
            else if(c[i]>0){
                if(flag==1){
                    error=true;
                    break;
                }
                else{
                    flag=2;
                    if(c[i]%2){
                        if(odd==0){
                            odd++;
                            odd_c=i+'a';
                        }
                        else{
                            error=true;
                            break;
                        }
                    }
                    c[i]/=2;
                    while(c[i]--){
                        ans[j]=i+'a';
                        j++;
                    }
                }
            }
        }
        int k;
        if(!error){
            if(odd){
                ans[j]=odd_c;
                k=j+1;
            }
            else
                k=j;
            for(int i=j-1;i>=0;i--){
                ans[k]=ans[i];
                k++;
            }
        }
        ans[k]='\0';
        if(error)
            cout<<"NO LUCK\n";
        else if(flag==0)
            cout<<"YES\n";
        else
            printf("%s\n",ans);
    }
}

