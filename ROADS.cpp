#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
#include<math.h>
#include<set>
#include<vector>
#include<cstring>
#include<string>
#include<map>
#include<climits>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
int V,k;
struct AdjListNode{
    int dest;
    int len;
    int price;
    AdjListNode* next;
};
struct AdjList{
    AdjListNode *head;
};
struct Graph{
    int V;
    AdjList* array;
};

struct AdjListNode* newAdjListNode(int dest,int l,int p){
    struct AdjListNode* newNode =
            (struct AdjListNode*) malloc(sizeof(struct AdjListNode));
    newNode->dest = dest;
    newNode->len = l;
    newNode->price=p;
    newNode->next = NULL;
    return newNode;
}

Graph* createGraph(int V){
    Graph* graph = (Graph*) malloc(sizeof(Graph));
    graph->V = V;
    graph->array = (AdjList*) malloc((V+1) * sizeof(AdjList));
    int i;
    for (i = 1; i <= V; ++i)
        graph->array[i].head = NULL;
    return graph;
}

void addEdge(Graph* graph, int src, int dest,int l,int p){
    AdjListNode* newNode = newAdjListNode(dest,l,p);
    newNode->next = graph->array[src].head;
    graph->array[src].head = newNode;
}

void printGraph(Graph* graph){
    int v;
    for (v = 1; v <= graph->V; ++v)
    {
        AdjListNode* pCrawl = graph->array[v].head;
        printf("\n Adjacency list of vertex %d\n head ", v);
        while (pCrawl)
        {
            printf("-> %d %d", pCrawl->dest,pCrawl->len);
            pCrawl = pCrawl->next;
        }
        printf("\n");
    }
}

queue<int> q;
int dist[200],expd[200];
void BFS(int node,Graph* graph){
    dist[node]=0;
    expd[node]=0;
    q.push(node);
    while(!q.empty()){
        int v=q.front();
        q.pop();
        AdjListNode* pCrawl = graph->array[v].head;
        while(pCrawl){
            if(dist[pCrawl->dest]==INT_MAX){
                expd[pCrawl->dest]=expd[v]+pCrawl->dest->price;
                if(expd[pCrawl->dest]<=k){
                    dist[pCrawl->dest]=dist[v]+pCrawl->dest->len;
                    q.push(pCrawl->dest);
                }
                else
                    expd[pCrawl->dest]=INT_MAX;
            }
            else{
                int temp
            }
            pCrawl = pCrawl->next;
        }
    }
}


int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        int e;
        scanf("%d%d%d",&k,&V,&e);
        Graph* graph = createGraph(V);
        int u,v,l,p;
        for(int i=0;i<e;i++){
            scanf("%d%d%d%d",&u,&v,&l,&p);
            addEdge(graph,u,v,l,p);
        }
        //printGraph(graph);
        for(int i=1;i<=V;i++){
            dist[i]=INT_MAX;
            expd[i]=INT_MAX;
        }
        expd=0;
        BFS(1,graph);
    }
    return 0;
}

