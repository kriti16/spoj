#include<iostream>
#include<stdio.h>
using namespace std;

int z(int);
int o(int);
int t(int);

int z(int n){
    if(n==1)
        return 1;
    return z(n-1)+o(n-1);
}

int o(int n){
    if(n==1)
        return 1;
    return o(n-1)+z(n-1)+t(n-1);
}

int t(int n){
    if(n==1)
        return 1;
    return t(n-1)+o(n-1);
}

int main(){
    int n;
    cin>>n;
    cout<<z(n)+o(n)+t(n)<<endl;
}
