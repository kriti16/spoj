#include<iostream>
#include<stdio.h>
#include<climits>
#include<stdlib.h>
using namespace std;

struct AdjListNode{
    int dest;
    int price;
    AdjListNode* next;
};
struct AdjList{
    AdjListNode *head;
};
struct Graph{
    int V;
    AdjList* array;
};
AdjListNode* newAdjListNode(int dest,int price){
    struct AdjListNode* newNode =
            (struct AdjListNode*) malloc(sizeof(struct AdjListNode));
    newNode->dest = dest;
    newNode->price=price;
    newNode->next = NULL;
    return newNode;
}
Graph* createGraph(int V){
    Graph* graph = (Graph*) malloc(sizeof(Graph));
    graph->V = V;
    graph->array = (AdjList*) malloc((V+1) * sizeof(AdjList));
    int i;
    for (i = 1; i <= V; ++i)
        graph->array[i].head = NULL;
    return graph;
}

void addEdge(Graph* graph, int src, int dest,int price){
    AdjListNode* newNode = newAdjListNode(dest,price);
    newNode->next = graph->array[src].head;
    graph->array[src].head = newNode;
}

int main(){
    int s;
    scanf("%d",&s);
    while(s--){
        int V,e,u,v,p;
        scanf("%d",&V);
        Graph* graph = createGraph(V);
        for(int i=1;i<=V;i++){
            scanf("%s%d",&e);
            for(int j=1;j<=e;j++){
                scanf("%d%d",&u,&p);
                addEdge(graph,i,j,p);
            }
        }

    }
}
