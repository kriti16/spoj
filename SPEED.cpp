#include<iostream>
#include<algorithm>
#include<cmath>
using namespace std;

int gcd(int m,int n){
    int a=max(m,n);
    int b=min(m,n);
    int r=a%b;
    while(r!=0){
        a=b;b=r;r=a%b;
    }
    return b;
}

int main(){
    int t,s1,s2;
    cin>>t;
    while(t--){
        cin>>s1>>s2;
        if(s1*s2<0){
            if(s1<0)s1*=-1;
            if(s2<0)s2*=-1;
            int ans=(s1+s2)/gcd((s1+s2),min(s1,s2));
            cout<<ans<<endl;
        }
        else{
            if(s1<0){
                s1*=-1;
                s2*=-1;
            }
            int ans=(max(s1,s2)-min(s1,s2))/gcd((max(s1,s2)-min(s1,s2)),min(s1,s2));
            cout<<ans<<endl;
        }
    }
}
