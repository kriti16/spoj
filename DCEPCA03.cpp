#include<iostream>
#include<stdio.h>
using namespace std;
typedef long long ll;
ll t[10005];

void compute(){
    for(int i=1;i<=10000;i++){
        t[i]=i;
    }

    for(int i=2;i<=10000;i++){
        if(t[i]==i){
            for(int k=1;i*k<=10000;k++){
                t[i*k]=t[i*k]/i*(i-1);
            }
        }
        t[i]+=t[i-1];
    }

}

int main(){
    compute();
    int T;
    cin>>T;
    while(T--){
        int n;
        cin>>n;
        printf("%lld\n",t[n]*t[n]);
    }
}
