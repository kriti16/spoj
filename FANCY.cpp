#include<iostream>
#include<stdio.h>
#include<string>
using namespace std;
int main(){
    int t,p;
    cin>>t;
    string num;
    while(t--){
        cin>>num;
        p=num.size();
        char c='a';
        for(int i=0;i<num.size();i++){
            if(num[i]!=c){
                p--;
                c=num[i];
            }
        }
        long long ans=1<<p;
        printf("%lld\n",ans);
    }
    return 0;
}
