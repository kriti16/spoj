#include<stdio.h>
typedef long long ll;
int fn[10000000]={0};
ll a[10000000];

void create_fn(){
    for(int i=2;i<=10000000;i++){
        if(fn[i]==0){
            for(int j=1;i*j<=10000000;j++)
                if(fn[i*j]==0)
                    fn[i*j]=i;
        }
    }
}

void create_val(){
    a[0]=a[1]=0;
    for(int i=2;i<=10000000;i++)
        a[i]=a[i-1]+fn[i];
}

int main(){
    create_fn();
    create_val();
    int t;
    scanf("%d",&t);
    int n;
    while(t--){
        scanf("%d",&n);
        printf("%lld\n",a[n]);
    }
    return 0;
}
