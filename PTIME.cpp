#include<stdio.h>
#include<vector>
#include<stdlib.h>
#include<string.h>
using namespace std;
typedef unsigned long long ull;
vector<int> prime;
void create_prime(){                    // primes less than sqrt(10^9)s
	bool is_prime[10001];
	memset(is_prime,true,10001);
	is_prime[0] = is_prime[1] = false;
	for(int i=2; i<=10000; i++){
		if(is_prime[i]){
			for(int j = i*i; j<=10000; j=j+i){
				is_prime[j] = false;
			}
			prime.push_back(i);
		}
	}
}

int main(){
    int n,i,temp;
    scanf("%d",&n);
    create_prime();
    for(i=0;prime[i]<=n && i<prime.size();i++){
        int temp=n;
        int q=temp/prime[i],p=0;
        while(q!=0){
            p+=q;
            temp=q;
            q=temp/prime[i];
        }
        if(prime[i]==2)
            printf("2^%d",p);
        else
            printf(" * %d^%d",prime[i],p);
    }
    printf("\n");
    return 0;
}
