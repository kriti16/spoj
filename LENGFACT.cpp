#include<stdio.h>
#include<cmath>
#include<iostream>

int pow(int n,int p){
    if(p==0)
        return 1;
    int temp=pow(n,p/2);
    if(p%2)
        return temp*temp*n;
    else
        return temp*temp;
}

using namespace std;
int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        int dig=(int)(log10(n)+0.0001)+1;
        cout<<"dig:"<<dig<<endl;
        int nine=9;
        long long ans=0;
        for(int i=1;i<dig;i++){
            ans+=((i-1)*nine);
            nine*=10;
            cout<<ans;
        }
        ans+=((dig-1)*(1+n-pow(10,dig-1)));
        ans++;
        printf("%lld\n",ans);
    }
    return 0;
}
