#include<iostream>
#include<stdio.h>
#include<string>
#include<sstream>
#include<algorithm>
using namespace std;
int main(){
    int t;
    cin>>t;
    while(t--){
        string s1,t1,s2,s3,t2;
        cin>>s1>>t1>>s2>>t2>>s3;
        //cout<<s1<<" "<<t1<<" "<<s2<<" "<<t2<<" "<<s3;
        int flag=0,m;
        m=max(s1.size(),s2.size());
        if(m<s3.size())
            m=s3.size();
        for(int i=0;i<m;i++){
            if(i<s1.size() && s1[i]=='m'){
                flag=1;
                break;
            }
            if(i<s2.size() && s2[i]=='m'){
                flag=2;
                break;
            }
            if(i<s3.size() && s3[i]=='m'){
                flag=3;
                break;
            }
        }
        if(flag==1){
            stringstream ss1,ss2,ss3;
            int n1,n2,n3;
            ss2<<s2;ss2>>n2;
            ss3<<s3;ss3>>n3;
            n1=n3-n2;
            ss1<<n1;ss1>>s1;
        }
        if(flag==2){
            stringstream ss1,ss2,ss3;
            int n1,n2,n3;
            ss1<<s1;ss1>>n1;
            ss3<<s3;ss3>>n3;
            n2=n3-n1;
            ss2<<n2;ss2>>s2;
        }
        if(flag==3){
            stringstream ss1,ss2,ss3;
            int n1,n2,n3;
            ss2<<s2;ss2>>n2;
            ss1<<s1;ss1>>n1;
            n3=n1+n2;
            ss3<<n3;ss3>>s3;
        }
        cout<<s1<<" + "<<s2<<" = "<<s3<<endl;
    }
    return 0;
}
