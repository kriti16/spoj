#include<iostream>
#include<stdio.h>
#include<algorithm>
using namespace std;

long long C(int n,int r){
    if(r==0)
        return 1;
    else
        return n*C(n-1,r-1)/r;
}

int main(){
    int n,len[2010];
    cin>>n;
    while(n!=0){
        for(int i=1;i<=n;i++)
            cin>>len[i];
        sort(len,len+n+1);
        long long ans=0;
        for(int i=1;i<n-1;i++){
            int k=i+2;
            for(int j=i+1;j<n;j++){
                int sum=len[i]+len[j];
                ans+=(k-j-1);
                while(k<=n && len[k]<=sum){
                    k++;
                    ans++;
                }
            }
        }
        ans=C(n,3)-ans;
        printf("%lld\n",ans);
        cin>>n;
    }
}
