#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
#include<math.h>
#include<set>
#include<vector>
#include<cstring>
#include<string>
#include<map>
#include<climits>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
struct node{
    ll val;
    int index;
};
node num[10005];

bool compare(node lhs, node rhs) { return lhs.val < rhs.val; }

int main(){
    int n,id[10005];
    ll l[10005],r[10005],diff=0;
    scanf("%d",&n);
    for(int i=1;i<=n;i++){
        scanf("%lld",&(num[i].val));
        num[i].index=i;
    }
    sort(num+1,num+n+1,compare);
    int L=1,R=0;
    l[0]=num[n].index;
    diff=num[n].val;
    for(int i=n-1;i>0;i--){
        if(diff>0){
            diff-=(num[i].val);
        }
        else{
            l[L]=num[i].index;
            L++;
            diff+=(num[i].val);
        }
    }
    sort(l,l+L);
    for(int i=0;i<L;i++)
        printf("%d\n",l[i]);
    return 0;
}

