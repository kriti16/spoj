#include<iostream>
#include<stdio.h>
#include<math.h>
using namespace std;
typedef long long ll;
int x[1000005],y[1000005];
ll mix,sumx,sumy;
int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        int n;
        scanf("%d",&n);
        for(int i=0;i<n;i++){
            scanf("%d%d",&x[i],&y[i]);
        }
        x[n]=x[0];
        y[n]=y[0];
        ll a=0,cx=0,cy=0;
        for(int i=0;i<n;i++){
            sumx=x[i]+x[i+1];
            sumy=y[i]+y[i+1];
            mix=x[i]*y[i+1]-x[i+1]*y[i];
            a+=mix;
            cx+=(sumx*mix);
            cy+=(sumy*mix);
        }
        double area=1.0*a/2;
        if(area<0){
            area=-area;
            cx=-cx;
            cy=-cy;
        }
        double ax=cx/area/6,ay=cy/area/6;
        printf("%.2lf %.2lf\n",ax,ay);
    }
}
