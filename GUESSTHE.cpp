#include<stdio.h>
#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;

long long gcd(long long m,long long n){
    long long a=max(m,n);
    long long b=min(m,n);
    long long r=a%b;
    while(r!=0){
        a=b;b=r;r=a%b;
    }
    return b;
}

long long lcm(long long a,long long b){
    return a*b/gcd(a,b);
}

int main(){
    char s[30];
    cin>>s;
    while(s[0]!='*'){
        long long ans=1;
        for(long long i=0;i<strlen(s);i++){
            if(s[i]=='Y')
                ans=lcm(ans,i+1);
        }
        int i;
        for(i=0;i<strlen(s);i++){
            if(s[i]=='N'){
                if(ans%(i+1)==0){
                    cout<<"-1\n";
                    break;
                }
            }
        }
        if(i==strlen(s))
            printf("%lld\n",ans);
        cin>>s;
    }
}
