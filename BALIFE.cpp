#include<stdio.h>
#include<algorithm>
#include<iostream>
#include<cmath>
using namespace std;
typedef long long ll;
ll a[10000];
ll steps(int s,int e,ll req){
    //cout<<req;
    if(s==e)
        return 0;
    if(req>a[s]){
        a[s+1]-=(req-a[s]);
        return max(req-a[s],steps(s+1,e,req));
    }
    else{
        a[s+1]+=(a[s]-req);
        return max(a[s]-req,steps(s+1,e,req));
    }
}

int main(){
    int n;
    scanf("%d",&n);
    while(n!=-1){
        ll sum=0;
        for(int i=1;i<=n;i++){
            scanf("%lld",&a[i]);
            sum+=a[i];
        }
        if(sum%n==0)
            printf("%lld\n",steps(1,n,sum/n));
        else
            printf("-1\n");
        scanf("%d",&n);
    }
}
