#include<stdio.h>
#include<cmath>
#include<iostream>
using namespace std;
int main(){
    int t,n1,n2,k,m;
    cin>>t;
    while(t--){
        cin>>k;
        int l,j=k;
        l=j;
        n1=(int)((sqrt(8*k+1)+1)/4);
        if(!(fabs(n1-((1+sqrt(8*k+1))/4))<0.00001))
            n1++;
        //cout<<n1<<endl;
        k-=(n1-1)*(2+(n1-2)*4)/2;
        //cout<<k<<endl;
        if(k<=(2*n1-1))
            n1=k;
        else
            n1=4*n1-2-k;
        //cout<<n1<<endl;

        n2=(int)((sqrt(8*j+1)-1)/4);
        if(!(fabs(n2-((sqrt(8*j+1)-1)/4))<0.00001))
            n2++;
        //cout<<n2<<endl;
        j-=(n2-1)*(6+(n2-2)*4)/2;
        //cout<<j<<endl;
        if(j<=(2*n2))
            n2=j;
        else
            n2=4*n2-j;
        //cout<<n2<<endl;
        printf("TERM %d IS %d/%d\n",l,n1,n2);

    }
    return 0;
}
