#include<iostream>
#include<stdio.h>
#include<algorithm>
using namespace std;

long long a[200000],temp[200000];
int n;

long long combo(int left,int right){
    int mid=(left+right)/2;
    int i=left,j=mid+1;
    long long count3=0;
    for(int k=left;k<=right;k++)
        temp[k]=a[k];
    sort(temp+left,temp+mid+1);
    sort(temp+mid+1,temp+right+1);
    while(i<=mid && j<=right){
        if(temp[i]>temp[j]){
            count3+=(mid-i+1);j++;
        }
        else
            i++;
    }
    return count3;
}

long long inv(int left,int right){
    long long count1=0,count2=0,count3=0;
    int mid=(left+right)/2;
    if(left>=right)
        return 0;
    else{
        count1=inv(left,mid);
        //printf("c1:%lld ",count1);
        count2=inv(mid+1,right);
        //printf("c2:%lld ",count2);
        count3=combo(left,right);
        //printf("c3:%lld\n",count3);
        return count1+count2+count3;
    }
}

int main(){
    long long t;
    scanf("%lld",&t);
    while(t--){
        scanf("%d",&n);
        for(int i=0;i<n;i++)
            scanf("%lld",&a[i]);
        printf("%lld\n",inv(0,n-1));
        //scanf("%d",&n);
    }
    return 0;
}
