#include<iostream>
#include<stdio.h>
#include<algorithm>
using namespace std;
int main(){
    int t,m[1005],w[1005];
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        for(int i=0;i<n;i++)
            cin>>m[i];
        for(int i=0;i<n;i++)
            cin>>w[i];
        sort(m,m+n);
        sort(w,w+n);
        long long sum=0;
        for(int i=0;i<n;i++)
            sum+=(m[i]*w[i]);
        printf("%lld\n",sum);
    }
}
