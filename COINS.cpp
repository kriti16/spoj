#include<cstdio>
#include<iostream>
#include<algorithm>
#include<map>
using namespace std;
typedef long long ll;
//int opt[500000001];
struct node{
    bool check;
    ll val;
};

map<ll,node>m;

ll opt(ll n){
    if(n<=1)
       return n;
    else{
        if(m[n].check==false){
            m[n].val=max(n,opt(n/2)+opt(n/3)+opt(n/4));
            m[n].check=true;
        }
        return m[n].val;
    }
}

int main(){
    m[0].val=0;m[1].val=1;
    m[0].check=true;m[1].check=true;
//int t=10;
  //  while(t--){
        ll n;
        cin>>n;
        cout<<opt(n)<<endl;
  //  }
        return 0;
}

