#include<stdio.h>
#include<string.h>
int is_prime[1000001];
int ans[1000001]={0};

int C(int n,int r){
    if(r==0 || n==r)
        return 1;
    return n*C(n-1,r-1)/r;
}

void create_prime(){
	for(int i=1; i<=1000000; i++){
        is_prime[i]=1;
	}
	is_prime[0] = is_prime[1] = 0;
	for(int i=2; i<=1000; i++){
		if(is_prime[i]){
			for(int j = 2; i*j<=1000000; j++){
				is_prime[i*j] = 0;
			}
		}
	}
	for(int i=1; i<=1000000; i++){
        ans[i]=ans[i-1]+is_prime[i];
	}
/*	for(int i=1; i<=10; i++){
        printf("%d ",is_prime[i]);//=ans[i-1]+is_prime[i];
	}  */
}

int main(){
    create_prime();
    int t;
    scanf("%d",&t);
    while(t--){
        int n;
        scanf("%d",&n);
        int sum=0;
        for(int i=0;i<=n/4;i++){
            sum+=C(n-3*i,i);
        }
        for(int i=1; i<=sum; i++){
       // printf("%d %d\n",i,ans[sum]);
	}
    printf("%d\n",ans[sum]);
    }
}
