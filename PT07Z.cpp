#include<iostream>
#include<stdio.h>
#include<queue>
#include<climits>
#include<stdlib.h>
using namespace std;
struct AdjListNode{
    int dest;
    AdjListNode* next;
};
struct AdjList{
    AdjListNode *head;
};
struct Graph{
    int V;
    AdjList* array;
};

struct AdjListNode* newAdjListNode(int dest){
    struct AdjListNode* newNode =
            (struct AdjListNode*) malloc(sizeof(struct AdjListNode));
    newNode->dest = dest;
    newNode->next = NULL;
    return newNode;
}

Graph* createGraph(int V){
    Graph* graph = (Graph*) malloc(sizeof(Graph));
    graph->V = V;
    graph->array = (AdjList*) malloc((V+1) * sizeof(AdjList));
    int i;
    for (i = 1; i <= V; ++i)
        graph->array[i].head = NULL;
    return graph;
}

void addEdge(Graph* graph, int src, int dest){
    AdjListNode* newNode = newAdjListNode(dest);
    newNode->next = graph->array[src].head;
    graph->array[src].head = newNode;
    newNode = newAdjListNode(src);
    newNode->next = graph->array[dest].head;
    graph->array[dest].head = newNode;
}
int max_dist_node;
queue<int> q;
int dist[10005];
void BFS(int node,Graph* graph){
    dist[node]=0;
    q.push(node);
    while(!q.empty()){
        int v=max_dist_node=q.front();
        q.pop();
        AdjListNode* pCrawl = graph->array[v].head;
        while(pCrawl){
            if(dist[pCrawl->dest]==INT_MAX){
                dist[pCrawl->dest]=dist[v]+1;
                q.push(pCrawl->dest);
            }
            pCrawl = pCrawl->next;
        }
    }
}

int main(){
    int n,u,v;
    scanf("%d",&n);
    Graph* graph = createGraph(n);
    for(int i=1;i<n;i++){
        scanf("%d%d",&u,&v);
        addEdge(graph,u,v);
    }
    for(int i=1;i<=n;i++)
        dist[i]=INT_MAX;
    BFS(1,graph);
    //cout<<dist[max_dist_node];
    for(int i=1;i<=n;i++)
        dist[i]=INT_MAX;
    BFS(max_dist_node,graph);
    printf("%d\n",dist[max_dist_node]);
}
