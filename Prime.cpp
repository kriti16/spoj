#include<stdio.h>
#include<vector>
#include<stdlib.h>
#include<string.h>
using namespace std;
typedef long long ll;
vector<ll> prime;
ll ans[40000];
int len2=0;
void create_prime(){                    // primes less than sqrt(10^9)s
	bool is_prime[40000];
	memset(is_prime,true,40000);
	is_prime[0] = is_prime[1] = false;
	for(ll i=2; i<=1000000000; i++){
		if(is_prime[i]){
			for(ll j = i*i; j<=1000000000; j=j+i){
				is_prime[j] = false;
			}
			prime.push_back(i);
		}
	}
}

void check(ll m,ll n){
    int k=0;
    ll i,j;
    //if(m==0)m=m+2;
    if(m==1)m++;
    for(i=m;i<=n;i++){
            int flag=0;
        for(j=0;prime[j]*prime[j]<=i && j<prime.size();j++){
            if(i%prime[j]==0){
                flag=1;
                break;
            }
        }
        if(flag==0){
            ans[k]=i;
            k++;
        }
    }
    len2=k;
}

int main(){
    create_prime();
    int t;
    scanf("%d",&t);
    ll m,n;
    while(t--){
        len2=0;
        scanf("%lld %lld",&m,&n);
        check(m,n);
        //printf("%d\n",len2);
        for(int i=0;i<len2;i++)
            printf("%lld\n",ans[i]);
    }
    return 0;
}


