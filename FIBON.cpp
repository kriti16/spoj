#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
#include<math.h>
#include<set>
#include<vector>
#include<cstring>
#include<string>
#include<map>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;

void multiply(ll F[2][2], ll M[2][2])
{
    ll x =  F[0][0] * M[0][0] + F[0][1] * M[1][0];
    ll y =  F[0][0] * M[0][1] + F[0][1] * M[1][1];
    ll z =  F[1][0] * M[0][0] + F[1][1] * M[1][0];
    ll w =  F[1][0] * M[0][1] + F[1][1] * M[1][1];
    F[0][0] = x;
    F[0][1] = y;
    F[1][0] = z;
    F[1][1] = w;
}

void power(ll F[2][2], ll n)
{
    if (n == 1)
        return;
    if(n == 0){
        F[0][0]=1;F[0][1]=0;F[1][0]=0;F[1][1]=1;
        return;
    }
    ll M[2][2] = {{1,1},{1,0}};
    power(F, n / 2);
    multiply(F, F);
    if (n % 2 != 0)
        multiply(F, M);
    //printf("%lld\n",F[1][1]);
}

ll fibo(ll n)
{
    ll F[2][2] = {{1,1},{1,0}};
    if (n == 0)
        return 0;
    power(F, n - 1);
    //cout<<"fib:"<<n<<" "<<F[0][0]<<endl;
    return F[0][0];
}

int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        ll n;
        scanf("%lld",&n);
        printf("%lld\n",fibo(n));
    }
    return 0;
}

