#include<stdio.h>
#include<math.h>
using namespace std;
typedef long long ll;
ll prime[40000];
ll ans[40000];
int len1,len2;
void create_prime(){
    prime[0]=2;
    int k=1,j;
    for(ll i=3;i<40000;i++){
        for(j=0;j<k;j++){
            if(i%prime[j]==0){
                break;
            }
        }
        if(j==k){
            prime[k]=i;
            k++;
        }
    }
    len1=k;
}

void check(ll m,ll n){
    int k=0;
    ll i,j,limit;
    for(i=m;i<=n;i++){
        limit=(ll)sqrt(i);
        //printf("limit:%lld\n",limit);
        for(j=0;prime[j]<=limit;j++){
            if(i%prime[j]==0)
                break;
        }
        if(prime[j]>limit){
            ans[k]=i;
            k++;
        }
    }
    len2=k;
}

int main(){
    create_prime();
    int t;
    scanf("%d",&t);
    ll m,n;
    while(t--){
        len2=0;
        scanf("%lld %lld",&m,&n);
        check(m,n);
        printf("%d\n",len2);
        for(int i=0;i<len2;i++)
            if(ans[i]!=1)
                printf("%lld\n",ans[i]);
    }
    return 0;
}

