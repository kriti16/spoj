#include<iostream>
#include<cstdio>
#include<stack>
#include<string>
#include<map>
#include<cstring>
#include <sstream>
using namespace std;

int main(){
    int t;
    cin>>t;
    map<char,int> mi;
    map<char,int> mo;
    mi['+']=mo['+']=0;
    mi['-']=mo['-']=1;
    mi['*']=mo['*']=2;
    mi['/']=mo['/']=3;
    mi['^']=mo['^']=4;
    mi['(']=-1;mo['(']=5;
    mo[')']=-1;

    while(t--){
        char c[401];
        scanf("%s",c);
        stack<string> opd;
        stack<char> opt;
        for(int i=0;i<strlen(c);i++){
            if(c[i]>='a' && c[i]<='z'){
                stringstream ss;
                ss << c[i];
                string s;
                ss>>s;
                opd.push(s);
            }
            else{
                if(opt.empty())
                    opt.push(c[i]);
                else{
                    if(mo[c[i]]>mi[opt.top()])
                        opt.push(c[i]);
                    else{
                        while(mo[c[i]]<mi[opt.top()]){
                            string temp1=opd.top();
                            opd.pop();
                            stringstream ss;
                            ss << opt.top();
                            string s;
                            ss>>s;
                            opd.top().append(temp1);
                            opd.top().append(s);
                            opt.pop();
                            if(opt.top()=='('){
                                opt.pop();
                                break;
                            }
                        }
                    }
                }
            }
        }
        cout<<opd.top()<<endl;
    }
    return 0;
}
