#include<iostream>
#include<stdio.h>
#include<queue>
#include<cstring>
#include<map>
using namespace std;
struct pt{
    int x;
    int y;
};

int dist[10][10];

int BFS(pt a,pt b,queue<pt>q){
    dist[a.x][a.y]=0;
    q.push(a);
    pt poss[8]={{1,2},{2,1},{2,-1},{1,-2},{-1,-2},{-2,-1},{-2,1},{-1,2}};
    while(!q.empty()){
        pt v=q.front();
        //cout<<"V:"<<v.x<<" "<<v.y<<": ";
        q.pop();
	for(int i=0;i<8;i++){
            pt temp;
            temp.x=v.x+poss[i].x;
            if((temp.x)>8 || (temp.x)<=0)
                continue;
            temp.y=v.y+poss[i].y;
            if((temp.y)>8 || (temp.y)<=0)
                continue;
            //cout<<"Neigh"<<temp.x<<" "<<temp.y<<"Dist:";
	    //cout<<dist[temp]<<endl;
            if(dist[temp.x][temp.y]==-1){
                dist[temp.x][temp.y]=dist[v.x][v.y]+1;
                q.push(temp);
                //cout<<dist[temp]<<endl;
                if(temp.x==b.x && temp.y==b.y)
                    return dist[temp.x][temp.y];
            }
        }
    }
}

int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        queue<pt> q;
        char c1,c2;
        pt a,b;
        scanf("\n%c%d %c%d",&c1,&a.y,&c2,&b.y);
        a.x=c1-'a'+1;
        b.x=c2-'a'+1;
        for(int i=1;i<=8;i++){
            for(int j=1;j<=8;j++){
                dist[i][j]=-1;
            }
        }
	if(a.x==b.x && a.y==b.y)
	  printf("0\n");
else
        printf("%d\n",BFS(a,b,q));
    }
}
