#include<iostream>
#include<stdio.h>
using namespace std;
int main(){
    int p;
    scanf("%d",&p);
    while(p--){
        int t,base,x,y,a,b,temp,num=0,xo;
        scanf("%d%d%d%d",&t,&base,&x,&y);
        temp=base;
        a=max(x,y);
        b=min(x,y);
        while(a/temp>=base)
            temp*=base;
        while(temp){
            xo=(a/temp+b/temp)%base;
            //cout<<"a:"<<a<<" b:"<<b<<" temp:"<<temp<<endl;
            num+=temp*xo;
            temp/=base;
        }
        printf("%d %d\n",t,num);
    }
}
