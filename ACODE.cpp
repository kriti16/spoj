#include<iostream>
#include<stdio.h>
#include<cstring>
using namespace std;
long long fibo[5005];

void compute(){
    fibo[0]=1;
    fibo[1]=1;
    fibo[2]=2;
    for(int i=3;i<=5001;i++)
        fibo[i]=fibo[i-1]+fibo[i-2];
}

int main(){
    compute();
    char s[5005];
    scanf("%s",s);
    while(s[0]!='0'){
        long long ans=1;
        int len=1;
        for(int i=1;i<strlen(s);i++){
            if(s[i]=='0'){
                ans*=fibo[len-1];
                len=0;
                continue;
            }
            else{
                if(s[i-1]=='1'){
                    len++;
                    continue;
                }
                if(s[i-1]=='0'){
                    len=1;
                    continue;
                }
                if(s[i-1]=='2'){
                    if(s[i]<='6'){
                        len++;
                        continue;
                    }
                    else{
                        ans*=fibo[len];
                        len=1;
                        continue;
                    }
                }
                if(s[i-1]>'2'){
                    ans*=fibo[len];
                    len=1;
                    continue;
                }
            }
        }
        if(s[strlen(s)-1]!='0')
            ans*=fibo[len];
        printf("%lld\n",ans);
        scanf("%s",s);
    }
}
