#include<stdio.h>
#include<algorithm>
using namespace std;
int a[100002];
int n,c;

bool valid(int v){
    int f=1,poss=a[0];
    for(int i=1;i<n;i++){
        if(a[i]-poss>=v){
            f++;
            poss=a[i];
        }
        if(f==c)
            return true;
    }
    return false;
}

int bs(int l,int r){
    int mid=l+(r-l+1)/2;
    if(l==r)return mid;
    else{
        if(valid(mid)){
            //printf("valid %d\n",mid);
            return bs(mid,r);
        }
        else{
            //printf("invalid %d\n",mid);
            return bs(l,mid-1);
        }
    }
}

int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        scanf("%d %d",&n,&c);
        for(int i=0;i<n;i++)
            scanf("%d",&a[i]);
        sort(a,a+n);
        printf("%d\n",bs(1,a[n-1]-a[0]));
    }
    return 0;
}
