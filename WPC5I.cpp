#include<stdio.h>
#include<algorithm>
#include<vector>
#include<string.h>
#include<iostream>
using namespace std;
typedef unsigned long long ull;

vector<int> prime;
void create_prime(){
	bool is_prime[50000];
	for(int i=0;i<50000;i++){
        is_prime[i]=true;
	}
	is_prime[0] = is_prime[1] = false;
	for(int i=2; i<50000; i++){
		if(is_prime[i]){
			for(int j = 2; i*j<50000; j++){
				is_prime[i*j] = false;
			}
			prime.push_back(i);
		}
	}
	//for(int i=0;i<10;i++){printf("%d ",prime[i]);}
}

int gcd(int m,int n){
    int a=max(m,n);
    int b=min(m,n);
    int r=a%b;
    while(r!=0){
        a=b;b=r;r=a%b;
    }
    return b;
}

ull power(int a,int p){
    if(p==0)return 1;
    if(p==1)return ((ull)a);
    ull temp=power(a,p/2);
    //printf("%llu\n",temp);
    ull ans=temp*temp;
    if(p%2==0)return ans;
    else return ans*a;
}

int main(){
    create_prime();
    int t,m,n;
    scanf("%d",&t);
    while(t--){
        scanf("%d %d",&m,&n);
        int r=gcd(m,n),p1,p2;
        //cout<<r<<endl;
        ull k=1;
        for(int i=0;prime[i]<=r && i<prime.size();i++){
            if(r%prime[i]==0){
                p1=0;p2=0;
                while(m%prime[i]==0){
                    p1++;
                    m/=prime[i];
                }
                while(r%prime[i]==0){
                    r/=prime[i];
                }
                while(n%prime[i]==0){
                    p2++;
                    n/=prime[i];
                }
                if(p1!=p2){
                    k*=power(prime[i],max(p1,p2));
                    //cout<<prime[i]<<" "<<k<<endl;
                }
            }
        }
        k*=(((ull)m/r)*n/r);
        printf("%llu\n",k);
    }
}
