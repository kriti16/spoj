#include<iostream>
#include<cstdio>
#include<queue>
using namespace std;
typedef long long ll;
#define m 1000000007

class mycomparision
{
  bool reverse;
public:
bool mycomparison(const bool& revparam=false)
    {reverse=revparam;}
  bool operator() (const int& lhs, const int&rhs) const
  {
    if (reverse) return (lhs<rhs);
    else return (lhs>rhs);
  }
};

int main(){
    int t,arr[200005];
    scanf("%d",&t);
    while(t--){
        int a,b,c,n,med,temp;
        priority_queue<int> q1;
        priority_queue<int,vector<int>,mycomparision> q2;
        ll ans=1;
        scanf("%d %d %d %d",&a,&b,&c,&n);
        arr[1]=1;
        q1.push(arr[1]);
        for(int i=2;i<=n;i++){
            if(q1.size()>=q2.size())
                med=q1.top();
            else
                med=q2.top();
            arr[i]=(int)((((ll)a)*med%m + ((ll)b)*i%m + c%m)%m);
            ans+=arr[i];
            if(arr[i]<q1.top()){
                q1.push(arr[i]);
                if((q1.size()-q2.size())>1){
                    q2.push(q1.top());
                    q1.pop();
                }
            }
            else{
                if(!q2.empty()){
                    if(arr[i]>q2.top()){
                        q2.push(arr[i]);
                        if((q2.size()-q1.size())>1){
                            q1.push(q2.top());
                            q2.pop();
                        }
                    }
                    else{
                        if(q1.size()-q2.size()==1)
                            q2.push(arr[i]);
                        else
                            q1.push(arr[i]);
                    }
                }

                else{
                    if(q1.size()-q2.size()==1)
                        q2.push(arr[i]);
                    else
                        q1.push(arr[i]);
                }
             }
            //printf("%d %d %d %d\n",q1.size(),q2.size(),q1.top(),q2.top());
        }
        printf("%lld\n",ans);
    }
}
