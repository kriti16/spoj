#include<iostream>
#include<stdio.h>
#include<string>
#include<sstream>
#include<stdlib.h>
#include<queue>
#include<climits>
#include<algorithm>
using namespace std;
bool num[10000];
int k=0;
struct node{
    int val;
    node* ptr;
};
node*(lst[10000]);
int prime[1061];
void create_prime(){
    int j;
    for(int i=2;i<10000;i++){
        if(num[i]==false){
            for(j=2;j*i<10000;j++)
                num[j*i]=true;
            if(i>1000){
                prime[k]=i;
                k++;
            }
        }
    }
}

node* add(int flag,int p,node* curr){
    if(flag==0){
        curr=(node*)malloc(sizeof(node));
        curr->val=p;
        curr->ptr=NULL;
    }
    else{
        curr->ptr=(node*)malloc(sizeof(node));
        curr=curr->ptr;
        curr->val=p;
        curr->ptr=NULL;
    }
    return curr;
}

node* neigh(int j){
    node *curr=NULL,*head=NULL;
    int flag=0,temp,b4;
    for(int i=0;i<4;i++){
        int mult=1;
        for(int x=1;x<4-i;x++)
            mult*=10;
        b4=prime[j]/mult;
        b4%=10;
        //cout<<b4<<endl;
        if(i!=0 && b4!=0){
            temp=prime[j]-b4*mult;
            //cout<<"temp:"<<temp<<endl;
            if(binary_search(prime,prime+k,temp)){
                if(!flag){
                    head=curr=add(0,temp,curr);
                    flag=1;
                }
                else
                    curr=curr->ptr=add(1,temp,curr);
            }
        }
        for(int y=1;y<=9;y++){
            if(y==b4)
                continue;
            temp=prime[j]+(y-b4)*mult;
            //cout<<"temp:"<<temp<<endl;
            if(binary_search(prime,prime+k,temp)){
                if(!flag){
                    head=curr=add(0,temp,curr);
                    flag=1;
                }
                else
                    curr=curr->ptr=add(1,temp,curr);
            }
        }
    }
    //cout<<"hi";
    return head;
}

void print(int j){
    printf("%d",prime[j]);
    node* curr=lst[prime[j]];
    while(curr->ptr!=NULL){
        printf("->%d",curr->val);
        curr=curr->ptr;
    }
    printf("\n");
}

void create_list(){
    for(int j=0;j<k;j++){
        lst[prime[j]]=neigh(j);
    }
    //for(int i=1;i<10;i++)
    //    print(i);
}

int ans;
int d[10000];
void BFS(int p1,int p2){
    queue<int> q;
    d[p1]=0;
    q.push(p1);
    if(p1==p2){
        ans=d[p1];
        return;
    }
    while(!q.empty()){
        int temp=q.front();
        node* curr=lst[temp];
        q.pop();
        while(curr!=NULL){
            if(d[curr->val]==INT_MAX){
                q.push(curr->val);
                d[curr->val]=d[temp]+1;
                if(curr->val==p2){
                    ans=d[curr->val];
                    return;
                }
            }
            curr=curr->ptr;
        }
    }
}

int main(){
    create_prime();
    create_list();
    int t,p1,p2,j;
    cin>>t;
    while(t--){
        ans=INT_MAX;
        cin>>p1>>p2;
        for(int i=0;i<10000;i++){
            d[i]=INT_MAX;
        }
        BFS(p1,p2);
        if(ans!=INT_MAX)
            cout<<ans<<endl;
        else
            cout<<"Impossible\n";
    }
    return 0;
}
