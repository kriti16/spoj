#include<iostream>
#include<stdio.h>
#include<stack>
using namespace std;
typedef long long ll;
struct bar{
  ll height;
  ll width;
};
int main(){
  int n;
  bar h[100005];
  scanf("%d",&n);
  while(n!=0){
    for(int i=0;i<n;i++){
      scanf("%lld",&h[i].height);
      h[i].width=1;
    }
    stack<bar> store;
    ll area=0;
    bar base;
    base.height=-1;
    base.width=0;
    store.push(base);
    for(int i=0;i<n;i++){
      if(store.top().height<h[i].height){
	store.push(h[i]);
      }
      else{
	while(h[i].height<store.top().height){
	  store.top().width+=h[i].width-1;
	  h[i].width=store.top().width+1;
	  if(area<store.top().height*store.top().width)
	    area=store.top().height*store.top().width;
	  store.pop();
	}
	if(h[i].height==store.top().height)
	  store.top().width+=h[i].width;
	else
	  store.push(h[i]);
      }
    }
    int temp=0;
    while(!store.empty()){
      store.top().width+=temp;
      if(area<store.top().height*store.top().width)
 	area=store.top().height*store.top().width;
      temp=store.top().width;
      store.pop();
    }
    printf("%lld\n",area);
    scanf("%d",&n);
  }
  return 0;
}


