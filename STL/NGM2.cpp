#include<iostream>
#include<stdio.h>
#include<vector>
using namespace std;
typedef long long ll;
ll gcd(ll a,ll b){
  ll ma,mi;
  ma=a>b ? a:b;
  mi=a>b ? b:a;
  ll r=ma%mi;
  while(r!=0){
    ma=mi;
    mi=r;
    r=ma%mi;
  }
  return mi;
}

ll lcm(ll a,ll b){
  return a/gcd(a,b)*b;
}

int main(){  
  ll n,k,num[20];
  scanf("%lld%lld",&n,&k);
  for(ll i=0;i<k;i++)
    scanf("%lld",&num[i]);
  vector<ll>plus,minus;
  vector<ll>::iterator ps;
  for(ll i=0;i<k;i++){
    plus.push_back(num[i]);
    ps=plus.end()-1;
    for(vector<ll>::iterator it=minus.begin();it<minus.end();it++)
      plus.push_back(lcm(num[i],*it));
    for(vector<ll>::iterator it=plus.begin();it<ps;it++)
      minus.push_back(lcm(num[i],*it));
  }
  ll sum=0;
  for(vector<ll>::iterator it=plus.begin();it<plus.end();it++)
    sum+=n/(*it);
  for(vector<ll>::iterator it=minus.begin();it<minus.end();it++)
    sum-=n/(*it);
  printf("%lld\n",n-sum);      
  }
