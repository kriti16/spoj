#include<iostream>
#include<stdio.h>
#include<map>
#include<cstring>
using namespace std;
int main(){
    int n,k;
    map<int,int> m;
    scanf("%d",&n);
    char cmd[10];
    int homo=0,hetero=0;
    while(n--){
        scanf("%s %d",cmd,&k);
        //cout<<cmd;
        if(!strcmp(cmd,"insert")){
            //cout<<"hi";
            if(m[k]==0)
                hetero++;
            else if(m[k]==1)
                homo++;
            m[k]++;
        }
        else{
            if(m[k]==2)
                homo--;
            else if(m[k]==1)
                hetero--;
            if(m[k]>0)
                m[k]--;
        }
        if(homo && hetero>1)
            printf("both\n");
        else if(homo)
            printf("homo\n");
        else if(hetero>1)
            printf("hetero\n");
        else
            printf("neither\n");
    }
    return 0;
}
