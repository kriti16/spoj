#include<iostream>
#include<stdio.h>
using namespace std;

int f[100001]={0};

void sieve(){
    for(int i=2;i<=100000;i++){
        if(f[i]==0){
            for(int j=2;i*j<=100000;j++)
                f[i*j]++;
        }
    }
    //for(int i=30;i<100;i++)
    //    cout<<i<<" "<<f[i]<<endl;
}

int main(){
    sieve();
    int t,n;
    cin>>t;
    while(t--){
        cin>>n;
        int p=0,i=30;
        while(p<n){
            if(f[i]>=3)
                p++;
            i++;
        }
        cout<<i-1<<endl;

    }
}
