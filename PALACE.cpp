#include<iostream>
#include<stdio.h>
using namespace std;
typedef long long ll;
#define M 98777

ll pow(int n,ll p){
    if(p==0)
        return 1;
    else{
        ll temp=pow(n,p/2);
        //cout<<temp<<endl;
        if(p%2)
            return temp*temp%M*n%M;
        else
            return temp*temp%M;
    }
}

int main(){
    int k;
    cin>>k;
    while(k--){
        ll n;
        scanf("%lld",&n);
        ll ans = pow(2,(n-1)*(n-1));
        printf("%lld\n",ans);
    }
}
