#include<iostream>
#include<cmath>
#include<stdio.h>
using namespace std;
int main(){
    int t;
    cin>>t;
    while(t--){
        int n,m,d,h;
        cin>>n>>m>>d;
        int sum=0;
        for(int i=0;i<n;i++){
            cin>>h;
            if(h%d)
                sum+=h/d;
            else
                sum+=(h/d-1);
        }
        //cout<<sum<<endl;
        if(sum>=m)
            cout<<"YES\n";
        else
            cout<<"NO\n";
    }
}
