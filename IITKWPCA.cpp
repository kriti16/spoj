#include<iostream>
#include<set>
#include<stdio.h>
#include<string>
#include<sstream>
using namespace std;

int main(){
    int t,flag=0;
    scanf("%d",&t);
    string temp;
    getline(cin,temp);
    while(t--){
        set<string> k;
        istringstream iss;
        string sentence,word;
        getline(cin, sentence);
        iss.str(sentence);
        while (iss>>word){
            k.insert(word);
        }
        printf("%d\n",k.size());
    }
    return 0;
}

