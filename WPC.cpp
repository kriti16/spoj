#include<cstdio>
#include<iostream>
#include<vector>
#include<string.h>
using namespace std;
typedef long long ll;
vector<ll> prime;
bool is_prime[1000001];

void create_prime(){
    memset(is_prime,true,1000001);
	is_prime[0] = is_prime[1] = false;
	for(ll i=2; i<1000000; i++){
		if(is_prime[i]){
			for(ll j = i*i; j<=1000000; j=j+i){
				is_prime[j] = false;
			}
			prime.push_back(i);
		}
	}
}

int main(){
    create_prime();
    int t;
    scanf("%d",&t);
    while(t--){
        ll n;
        scanf("%lld",&n);
        ll total=1,temp=n;
        for(int i=1;prime[i]*prime[i]<=n && i<prime.size();i++){
            int f=0;
            while(temp>0 && temp%prime[i]==0){
                f++;
                temp/=prime[i];
            }
            total*=(2*f+1);
            if(temp==1)
                break;
        }
        if(temp>1)
                total*=3;
        printf("%lld\n",total/2);
    }
    return 0;
}
