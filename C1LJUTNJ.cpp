#include<iostream>
#include<stdio.h>
#include<algorithm>
using namespace std;
#define gc getchar_unlocked
#define pc(x) putchar_unlocked(x);
typedef long long ll;

    inline void writeInt (ll n)
    {
        ll N = n, rev, count = 0;
        rev = N;
        if (N == 0) { pc('0'); pc('\n'); return ;}
        while ((rev % 10) == 0) { count++; rev /= 10;} //obtain the count of the number of 0s
        rev = 0;
        while (N != 0) { rev = (rev<<3) + (rev<<1) + N % 10; N /= 10;}  //store reverse of N in rev
        while (rev != 0) { pc(rev % 10 + '0'); rev /= 10;}
        while (count--) pc('0');
    }
void scanint(ll &x)
{
    register ll c = gc();
    x = 0;
    for(;(c<48 || c>57);c = gc());
    for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
}
int main(){
    ll m,n;
    scanint(m);scanint(n);
    //cout<<m<<" "<<n<<endl;
    ll w[100005];
    unsigned long long int eq[100005],sum=0,os=0;
    for(ll i=0;i<n;i++)
        scanint(w[i]);
    sort(w,w+n);
    ll i;
    for(i=n-2;i>=0;i--){
        eq[i]=(w[i+1]-w[i])*(n-1-i)+eq[i+1];
        //cout<<"eq:"<<i<<":"<<eq[i]<<endl;
        if(eq[i]>m)
            break;
    }
    m-=eq[i+1];
    sum+=w[i+1]-m/(n-1-i);
    ll j=m%(n-1-i);
    os+=sum*sum*(n-1-i-j);
    os+=((sum-1)*(sum-1)*j);
    for(ll k=0;k<=i;k++)
        os+=(w[k]*w[k]);
    writeInt(os);printf("\n");
}
