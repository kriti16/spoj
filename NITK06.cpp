#include<iostream>
#include<stdio.h>
using namespace std;
int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        int n,remain,curr;
        scanf("%d",&n);
        bool error=false;
        scanf("%d",&remain);
        for(int i=1;i<n;i++){
            scanf("%d",&curr);
            if(!error){
                if(curr-remain<0)
                    error=true;
                else
                    remain=curr-remain;
            }
        }
        if(error || remain)
            cout<<"NO\n";
        else
            cout<<"YES\n";
    }
}
