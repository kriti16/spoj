#include<cmath>
#include<iostream>
#include<stdio.h>
using namespace std;
int main(){
    int t;
    cin>>t;
    while(t--){
        double u, v, w, W, V, U,x,y,z,X,Y,Z,vol,a,b,c,d;
        scanf("%lf%lf%lf%lf%lf%lf",&U,&V,&w,&W,&v,&u);
        X=(w-U+v)*(U+v+w);
        x=(U-v+w)*(v-w+U);
        Y=(u-V+w)*(V+w+u);
        y=(V-w+u)*(w-u+V);
        Z=(v-W+u)*(W+u+v);
        z=(W-u+v)*(u-v+W);
        a=sqrt(x*Y*Z);//*sqrt(Z);
        b=sqrt(y*Z*X);//*sqrt(X);
        c=sqrt(z*X*Y);//*sqrt(Y);
        d=sqrt(x*y*z);//*sqrt(z);
        vol=sqrt(-a+b+c+d)*sqrt(a-b+c+d)*sqrt(a+b-c+d)*sqrt(a+b+c-d)/192/u/v/w;
        printf("%.4lf\n",vol);
    }
}
