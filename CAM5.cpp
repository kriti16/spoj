#include <stdio.h>
#include <stdlib.h>
#include<iostream>
using namespace std;
int label[100005];
bool visited[100005];
struct AdjListNode{
    int dest;
    AdjListNode* next;
};
struct AdjList{
    AdjListNode *head;
};
struct Graph{
    int V;
    AdjList* array;
};

struct AdjListNode* newAdjListNode(int dest){
    struct AdjListNode* newNode =
            (struct AdjListNode*) malloc(sizeof(struct AdjListNode));
    newNode->dest = dest;
    newNode->next = NULL;
    return newNode;
}

Graph* createGraph(int V){
    Graph* graph = (Graph*) malloc(sizeof(Graph));
    graph->V = V;
    graph->array = (AdjList*) malloc(V * sizeof(AdjList));
    int i;
    for (i = 0; i < V; ++i)
        graph->array[i].head = NULL;
    return graph;
}

void addEdge(Graph* graph, int src, int dest){
    AdjListNode* newNode = newAdjListNode(dest);
    newNode->next = graph->array[src].head;
    graph->array[src].head = newNode;
    newNode = newAdjListNode(src);
    newNode->next = graph->array[dest].head;
    graph->array[dest].head = newNode;
}

void printGraph(Graph* graph){
    int v;
    for (v = 0; v < graph->V; ++v)
    {
        AdjListNode* pCrawl = graph->array[v].head;
        printf("\n Adjacency list of vertex %d\n head ", v);
        while (pCrawl)
        {
            printf("-> %d", pCrawl->dest);
            pCrawl = pCrawl->next;
        }
        printf("\n");
    }
}

void DFS(int v,Graph* graph){
    visited[v]=true;
    AdjListNode* pCrawl = graph->array[v].head;
    while(pCrawl){
        if(visited[pCrawl->dest]==false){
            label[pCrawl->dest]=label[v];
            DFS(pCrawl->dest,graph);
        }
        pCrawl=pCrawl->next;
    }
}

int main()
{
    int t;
    scanf("%d",&t);
    while(t--){
        int V;
        scanf("%d",&V);
        Graph* graph = createGraph(V);
        int e,u,v;
        scanf("%d",&e);
        for(int i=0;i<e;i++){
            scanf("%d%d",&u,&v);
            addEdge(graph,u,v);
        }
        bool taken[100005];
        for(int i=0;i<V;i++){
            label[i]=i;
            visited[i]=false;
            taken[i]=false;
        }
        for(int i=0;i<V;i++){
            if(visited[i]==false)
                DFS(i,graph);
        }
        int dift=0;
        for(int i=0;i<V;i++){
            if(taken[label[i]]==false){
                taken[label[i]]=true;
                dift++;
            }
        }
        cout<<dift<<endl;
    }
    return 0;
}
