#include<iostream>
#include<stdio.h>
#define gc getchar_unlocked
#define pc(x) putchar_unlocked(x);
    inline void writeInt (long long n)
    {
        long long N = n, rev, count = 0;
        rev = N;
        if (N == 0) { pc('0'); pc('\n'); return ;}
        while ((rev % 10) == 0) { count++; rev /= 10;} //obtain the count of the number of 0s
        rev = 0;
        while (N != 0) { rev = (rev<<3) + (rev<<1) + N % 10; N /= 10;}  //store reverse of N in rev
        while (rev != 0) { pc(rev % 10 + '0'); rev /= 10;}
        while (count--) pc('0');
    }
using namespace std;
void scanint(int &x)
{
    register int c = gc();
    x = 0;
    for(;(c<48 || c>57);c = gc());
    for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
}
int a[10005];
int n;
long long dp[10005];
long long gc(int start){
    if(start==n-2)
        return max((long long)a[n-2],(long long)a[n-1]);
    if(start==n-1)
        return (long long)a[n-1];
    if(dp[start]==0)
        dp[start]=max(a[start]+gc(start+2),gc(start+1));
    return dp[start];
}

int main(){
    int t,num;
    scanint(t);
    for(int i=1;i<=t;i++){
        scanint(n);
        for(int i=0;i<n;i++)
            scanint(a[i]);
        for(int i=0;i<n;i++)
            dp[i]=0;
        if(n==0)
            printf("Case %d: 0\n",i);
        else{
            printf("Case %d: ",i);
            writeInt(gc(0));
            printf("\n");
        }
    }
}
