#include<stdio.h>
#include<algorithm>
#include<cmath>
#include<stdlib.h>
typedef long long ll;
using namespace std;

ll gcd(ll m,ll n){
    ll a=max(m,n);
    ll b=min(m,n);
    ll r=a%b;
    while(r!=0){
        a=b;b=r;r=a%b;
    }
    return b;
}

int main(){
    ll a,b;
    scanf("%lld%lld",&a,&b);
    while(a!=0 && b!=0){
        ll g=gcd(a,b);
        ll lcm=a/g*b;
        ll total= lcm/a*lcm/b;
        printf("%lld\n",total);
        scanf("%lld%lld",&a,&b);
    }
}
