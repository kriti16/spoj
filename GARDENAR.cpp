#include<stdio.h>
#include<cmath>
#define sqr 1.7320508075688772935274463415059
int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        int a,b,c;
        scanf("%d%d%d",&a,&b,&c);
        double s=1.0*(a+b+c)/2;
        double A=sqrt(s*(s-a)*(s-b)*(s-c));
        double area=(sqr*(a*a+b*b+c*c)/4+3*A)/2;
        //double area=sqr*(a+b+c)*(a+b+c)/12;
        printf("%.2lf\n",area);
    }
}
