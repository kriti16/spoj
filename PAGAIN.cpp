#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<ctime>
using namespace std;
typedef unsigned long long int ull;
ull pow(ull a,ull d,ull n){
    if(d==0)
        return 1;
    ull temp=pow(a,d/2,n);
    //cout<<"temp:"<<temp<<endl;
    if(d%2)
        return temp*temp%n*a%n;
    else
        return temp*temp%n;
}

bool mr_test(ull n){
    if(n==1)
        return false;
    if(n==2 || n==3)
        return true;
    if(n%2==0)
        return false;
    int times=2;
    ull a,prev=0;
    while(times--){
        int flag=0;
        ull temp=n-1;
        ull s=0,d;
        while(temp%2==0){
            temp/=2;
            s++;
        }
        d=(n-1)/(1<<s);
        //cout<<"n:"<<n<<" s:"<<s<<" d:"<<d<<endl;
        srand(time(0));
        a=rand()%(n-3)+2;
        while(a==prev)
            a=rand()%(n-3)+2;
        prev=a;
        //cout<<a<<endl;
        ull x=pow(a,d,n);
        //cout<<x<<endl;
        if(x==1 || x==n-1)
            flag=1;
        else{
            for(ull i=1;i<s;i++){
                x=(x*x)%n;
                if(x==n-1){
                    flag=1;
                    break;
                }
                if(x==1)
                    return false;
            }
        }
        if(flag==0)
            return false;
    }
    return true;
}

int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        ull n;
        scanf("%llu",&n);
        while(!mr_test(--n));
        printf("%llu\n",n);
    }
}
