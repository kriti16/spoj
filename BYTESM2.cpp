#include<iostream>
#include<stdio.h>
#include<algorithm>
using namespace std;
int mat[101][101];
int main(){
    int h,w,t;
    cin>>t;
    while(t--){
        cin>>h>>w;
        for(int i=0;i<h;i++){
            for(int j=0;j<w;j++)
                cin>>mat[i][j];
        }
        int ans=0,m;
        for(int i=1;i<h;i++){
            for(int j=0;j<w;j++){
                m=mat[i-1][j];
                if(j!=0)
                    m=max(m,mat[i-1][j-1]);
                if(j!=w-1)
                    m=max(m,mat[i-1][j+1]);
                mat[i][j]+=m;
                //cout<<"m:"<<m<<endl;
            }
        }
        for(int j=0;j<w;j++)
            ans=max(ans,mat[h-1][j]);
        cout<<ans<<endl;
    }
    return 0;
}
