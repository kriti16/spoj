#include<stdio.h>
#include<iostream>
using namespace std;

int main(){
    int n,np,p,coeff[100000],f=1;
    cin>>n;
    while(n!=-1){
        for(int i=n;i>=0;i--)
            cin>>coeff[i];
        cin>>np;
        int flag=0;
        for(int i=1;i<=np;i++){
            cin>>p;
            if(flag==0){
                cout<<"Case "<<f++<<":\n";
                flag=1;
            }
            long long ans=0,I=1;
            for(int i=0;i<=n;i++){
                ans+=(coeff[i]*I);
                I*=p;
            }
            printf("%lld\n",ans);
        }
        cin>>n;
    }
}
