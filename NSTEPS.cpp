#include<stdio.h>
#include<iostream>
using namespace std;
int main(){
    int t;
    cin>>t;
    while(t--){
        int x,y;
        cin>>x>>y;
        if(x==y){
            if(x%2==0)
                cout<<4*x/2<<endl;
            else{
                long long ans=1+((x+1)/2-1)*4;
                printf("%lld\n",ans);
            }
        }
        else if(y==(x-2)){
            if(y%2==0){
                long long ans=2+(y*2);
                cout<<ans<<endl;
            }
            else{
                long long ans=3+((y+1)/2-1)*4;
                printf("%lld\n",ans);
            }
        }
        else
            cout<<"No Number\n";
    }
}
