#include<iostream>
#include<stdio.h>
using namespace std;
long long f[15];
void compute(){
    f[0]=1;
    for(int i=1;i<15;i++)
        f[i]=i*f[i-1];
}

int main(){
    compute();
    int k,n,p;
    while(scanf("%d%d",&k,&n)!=EOF){
        long long ans=f[k];
        for(int i=0;i<n;i++){
            scanf("%d",&p);
            ans/=f[p];
        }
        printf("%lld\n",ans);
    }
    return 0;
}
