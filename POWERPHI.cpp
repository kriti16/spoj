#include<stdio.h>
#include<math.h>
typedef long long ll;
#define P 1000000007
void multiply(ll F[2][2], ll M[2][2])
{
    ll x =  F[0][0] * M[0][0] + F[0][1] * M[1][0];
    ll y =  F[0][0] * M[0][1] + F[0][1] * M[1][1];
    ll z =  F[1][0] * M[0][0] + F[1][1] * M[1][0];
    ll w =  F[1][0] * M[0][1] + F[1][1] * M[1][1];
    F[0][0] = x%P;
    F[0][1] = y%P;
    F[1][0] = z%P;
    F[1][1] = w%P;
}

void power(ll F[2][2], ll n)
{
    if(n == 0){
        F[0][0]=1;F[0][1]=0;F[1][0]=0;F[1][1]=1;
        return;
    }
    ll M[2][2] = {{1,1},{1,0}};
    power(F, n / 2);
    multiply(F, F);
    if (n % 2 != 0)
        multiply(F, M);
    //printf("%lld\n",F[1][1]);
}

ll fibo(ll n)
{
    ll F[2][2] = {{1,1},{1,0}};
    if (n == 0)
        return 1;
    if(n==1)
        return 2;
    if(n==2)
        return 3;
    if(n==3)
        return 4;
    power(F, n - 3);
    //cout<<"fib:"<<n<<" "<<F[0][0]<<endl;
    return F[0][0]*4%P+F[0][1]*3;
}

int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        ll n;
        scanf("%lld",&n);
        ll ans=fibo(n)%P;
        printf("%lld\n",ans);
    }
}
