#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
#include<vector>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
#define M 100000
int fibo[1100005];
void compute(){
    fibo[1]=0;
    fibo[2]=1;
    for(int i=3;i<=1100000;i++)
        fibo[i]=(fibo[i-1]+fibo[i-2])%M;
}

bool compare(int a,int b){
	return a>b;
}

int main(){
    compute();
    int t;
    scanf("%d",&t);
    for(int x=1;x<=t;x++){
        int a,b;
        scanf("%d %d",&a,&b);
        printf("Case %d:",x);
        vector<int> v(fibo+a,fibo+a+b+1);
        int lim=min(b+1,100);
        make_heap(v.begin(),v.end(),compare);
        while(lim--){
            printf(" %d",v.front());
            pop_heap(v.begin(),v.end(),compare);
            v.pop_back();
        }
        printf("\n");
    }
    return 0;
}
