#include<iostream>
#include<sstream>
#include<string>
#include<stdlib.h>
#include<stdio.h>
using namespace std;
int main(){
    int t;
    cin>>t;
    while(t--){
        scanf("\n");
        string input,word;
        getline(cin,input);
        istringstream iss;
        iss.str(input);
        int f=0,m=0,last=0,curr=0;
        while(iss>>word){
            //cout<<"word:"<<word<<endl;
            curr=word.size();
            //cout<<"curr:"<<curr<<" last:"<<last<<endl;
            if(curr==last){
                f++;
            }
            else{
                f=1;
            }
            if(m<f)
                m=f;
            //cout<<"f:"<<f<<endl;
            last=curr;
        }
        cout<<m<<endl;
    }
}
