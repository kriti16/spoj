#include<iostream>
#include<algorithm>
#include<stdio.h>
using namespace std;
long long prime[80000];
int len,total;
int K[50];
bool num[1000005];
void create_prime(){
    int k=0,j;
    for(int i=2;i<=1000000;i++){
        if(num[i]==false){
            prime[k]=i;k++;
            for(j=2;j*i<=1000000;j++)
                num[j*i]=true;
        }
    }
    len=k;
}

void compute(){
    K[0]=2;
    int k=1;
    for(int i=0;prime[i]<1000;i++){
        long long p=prime[i]*prime[i];
        //cout<<p<<endl;
        while(p<1000000){
            int sum=(p*prime[i]-1)/(prime[i]-1);
            //cout<<sum<<endl;
            if(binary_search(prime,prime+len,sum)){
                //cout<<"yes"<<endl;
                K[k]=p;
                //cout<<K[k]<<endl;
                k++;
            }
            p*=prime[i];
        }
    }
    total=k;
    //cout<<"total:"<<total<<endl;
    //for(int i=0;i<total;i++)
    //    cout<<K[i]<<" ";

}

int main(){
    create_prime();
    compute();
    int t;
    scanf("%d",&t);
    while(t--){
        int a,b;
        scanf("%d %d",&a,&b);
        int f=0;
        for(int i=0;i<=total;i++){
            if(K[i]>=a && K[i]<=b)
                f++;
        }
        printf("%d\n",f);
    }
}


