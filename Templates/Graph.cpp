int dist[10][10];

int BFS(pt a,pt b,queue<pt>q){
    dist[a.x][a.y]=0;
    q.push(a);
    pt poss[8]={{1,2},{2,1},{2,-1},{1,-2},{-1,-2},{-2,-1},{-2,1},{-1,2}};
    while(!q.empty()){
        pt v=q.front();
        //cout<<"V:"<<v.x<<" "<<v.y<<": ";
        q.pop();
	for(int i=0;i<8;i++){
            pt temp;
            temp.x=v.x+poss[i].x;
            if((temp.x)>8 || (temp.x)<=0)
                continue;
            temp.y=v.y+poss[i].y;
            if((temp.y)>8 || (temp.y)<=0)
                continue;
            //cout<<"Neigh"<<temp.x<<" "<<temp.y<<"Dist:";
	    //cout<<dist[temp]<<endl;
            if(dist[temp.x][temp.y]==-1){
                dist[temp.x][temp.y]=dist[v.x][v.y]+1;
                q.push(temp);
                //cout<<dist[temp]<<endl;
                if(temp.x==b.x && temp.y==b.y)
                    return dist[temp.x][temp.y];
            }
        }
    }
}
