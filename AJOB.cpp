#include<iostream>
#include<stdio.h>
using namespace std;
typedef long long ll;

void sol(ll a,ll b,ll& x,ll& y){
    if(a%b==0){
        x=0;
        y=1;
        return;
    }
    sol(b,a%b,x,y);
    ll temp=x;
    x=y;
    y=temp-y*(a/b);
}

ll inv(ll k,ll p){
    ll x,y;
    sol(k,p,x,y);
    if(x<0)
        x+=p;
    //cout<<"k:"<<k<<" p:"<<p<<" x:"<<x<<endl;
    return x;
}


ll Cd(ll n,ll k,ll p){
    if(k==0)
        return 1;
    if(n<k)
        return 0;
    return n*Cd(n-1,k-1,p)%p*inv(k,p)%p;
}

ll C(ll n,ll k,ll p){
    if(k==0)
        return 1;
    if(n<k)
        return 0;
    else{
        ll temp=p,ans=1;
        while(n/temp>=p)
            temp*=p;
        while(temp){
            if(n/temp>=p)
                ans*=C(n/temp,k/temp,p);
            else
                ans*=Cd(n/temp,k/temp,p);
            n%=temp;
            k%=temp;
            temp/=p;
            ans%=p;
        }
        return ans;
    }
}

int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        ll n,k,p;
        scanf("%lld %lld %lld",&n,&k,&p);
        n++;k++;
        printf("%lld\n",C(n,k,p));
    }
}
