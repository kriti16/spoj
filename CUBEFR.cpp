#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
#include<math.h>
#include<set>
#include<vector>
#include<cstring>
#include<string>
#include<map>
#include<climits>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
bool num[1000005];
int cf[900000];
int k;
void compute(){
    for(int i=1;i<=1000000;i++)
        num[i]=true;
    for(int i=2;i<=100;i++){
        int x=i*i*i;
        for(int j=1;x*j<=1000000;j++)
            num[x*j]=false;
    }
    k=1;
    for(int i=1;i<=1000000;i++){
        if(num[i]==true){
            cf[k]=i;
            k++;
        }
    }
    //cout<<k<<endl;
}

int BS(int left,int right,int key){
    if(left==right)
        return left;
    int mid=(left+right)/2;
    if(cf[mid]==key)
        return mid;
    else{
        if(cf[mid]>key)
            return BS(left,mid-1,key);
        else
            return BS(mid+1,right,key);
    }
}

int main(){
    compute();
    int t;
    scanf("%d",&t);
    for(int i=1;i<=t;i++){
        int n;
        scanf("%d",&n);
        printf("Case %d: ",i);
        if(binary_search(cf+1,cf+k,n)){
            printf("%d\n",BS(1,k-1,n));
        }
        else
            printf("Not Cube Free\n");
    }
    return 0;
}

