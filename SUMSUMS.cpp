#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
#include<math.h>
#include<set>
#include<vector>
#include<cstring>
#include<string>
#include<map>
#include<climits>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
#define M 98765431

void EE(int a,int b,ll& x,ll& y){
    if(a%b==0){
        x=0;
        y=1;
        return;
    }
    EE(b,a%b,x,y);
    ll temp = x;
    x = y;
    y = temp - y*(a/b);
}

ll inverse(int a,int m){
    ll x,y;
    EE(a,m,x,y);
    if(x<0)
        x+=m;
    return x%M;
}

ll pow(int a,int p){
    if(p==0)
        return 1;
    ll temp=pow(a,p/2);
    ll ans=temp*temp%M;
    if(p%2)
        ans*=a;
    return ans%M;
}

int main(){
    int n,t,num,v[50005];
    ll ans[50005];
    scanf("%d%d",&n,&t);
    ll sum=0;
    for(int i=0;i<n;i++){
        scanf("%d",&v[i]);
        sum=(sum+v[i])%M;
    }
    int m=M;
    if(n!=1){
        ll term;
        if(t%2)
            term=(1+pow(n-1,t))*sum%M*inverse(n,m)%M;
        else
            term=(pow(n-1,t)-1)*sum%M*inverse(n,m)%M;
        if(t%2)
            for(int i=0;i<n;i++)
                ans[i]=(term-v[i])%M;
        else
            for(int i=0;i<n;i++)
                ans[i]=(term+v[i])%M;
        for(int i=0;i<n;i++){
            if(ans[i]<0)
                ans[i]+=M;
            printf("%lld\n",ans[i]);
        }
    }
    else
        printf("0\n");
    return 0;
}
