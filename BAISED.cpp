#include<stdio.h>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
typedef long long ll;
bool place[1000005];
int pex[1000005],pleft[1000005];
int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        int n,c;
        scanf("%d",&n);
        for(int i=1;i<=n;i++)
            place[i]=false;
        int k=0;
        char name[100];
        for(int i=1;i<=n;i++){
            scanf("%s%d",name,&c);
            if(place[c]==false)
                place[c]=true;
            else{
                pex[k]=c;
                k++;
            }
        }
        int K=0;
        for(int i=1;i<=n;i++){
            if(!place[i]){
                pleft[K]=i;
                K++;
            }
        }
        sort(pleft,pleft+K);
        sort(pex,pex+k);
        long long diff=0;
        for(int i=0;i<k;i++)
            diff+=fabs(pleft[i]-pex[i]);
        printf("%lld\n",diff);
    }
}

