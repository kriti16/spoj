#include<cstdio>
#include<climits>
using namespace std;
typedef long long ll;
int num[105];
bool f[500501][105];
int n;
ll ans;
void Fnd(int mi,int mx){
    for(int i=0;i<=n;i++){
        f[0][i]=true;
        if(i!=0)
            f[i][0]=false;
    }
    for(int i=1;i<=mx;i++){
        for(int j=1;j<=n;j++){
            f[i][j] = f[i][j-1];
            if (i >= num[j-1])
                f[i][j] = f[i][j] || f[i-num[j-1]][j-1];
        }
    }
    for(int i=mi;i<=mx;i++){
        if(f[i][n])
            ans+=i;
    }
}

int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        scanf("%d",&n);
        int mx=0;
        int mi=INT_MAX;
        for(int i=0;i<n;i++){
            scanf("%d",&num[i]);
            mx+=num[i];
            if(num[i]<mi)
                mi=num[i];
        }
        ans=0;
        Fnd(mi,mx);
        printf("%lld\n",ans);
    }
}
