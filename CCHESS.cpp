#include<iostream>
#include<stdio.h>
#include<queue>
#include<climits>
#include<cstring>
#define IM 999999
using namespace std;
struct pt{
    int x;
    int y;
};
int price[10][10][10][10];
bool visited[10][10];
int dist[10][10];

void compute(){
 pt poss[8]={{1,2},{2,1},{2,-1},{1,-2},{-1,-2},{-2,-1},{-2,1},{-1,2}};
 for(int i=0;i<8;i++){
  for(int j=0;j<8;j++){
    for(int k=0;k<8;k++){
      for(int l=0;l<8;l++){
	  price[i][j][k][l]=IM;
      }
    }
    for(int m=0;m<8;m++){
      pt temp;
      temp.x=i+poss[m].x;
      if((temp.x)>7 || (temp.x)<0)
	continue;
      temp.y=j+poss[m].y;
      if((temp.y)>7 || (temp.y)<0)
	continue;
      price[i][j][temp.x][temp.y]=i*temp.x+j*temp.y;
    }
  }
 }
}

int BFS(pt a,pt b){
  visited[a.x][a.y]=true;
  price[a.x][a.y][a.x][a.y]=0;//int t=2;
  while(1){
    int least=IM;
    pt leastp;        
    for(int i=0;i<8;i++){
      for(int j=0;j<8;j++){
	if(visited[i][j]==false){
	  if(least>=price[a.x][a.y][i][j]){
	    least=price[a.x][a.y][i][j];
	    leastp.x=i;
	    leastp.y=j;
	  }
        }
	//cout<<least<<endl;
      }
    }
    visited[leastp.x][leastp.y]=true;
    //price[a.x][a.y][leastp.x][leastp.y]=price[a.x][a.y][leastp.x][leastp.y];
    //cout<<leastp.x<<" "<<leastp.y<<" dist:"<<price[a.x][a.y][leastp.x][leastp.y]<<endl;
    if(leastp.x==b.x && leastp.y==b.y)
      return price[a.x][a.y][leastp.x][leastp.y];
    for(int i=0;i<8;i++){
      for(int j=0;j<8;j++){
	if(visited[i][j]!=true)
	  price[a.x][a.y][i][j]=min(price[a.x][a.y][i][j],price[a.x][a.y][leastp.x][leastp.y]+price[leastp.x][leastp.y][i][j]);
      }
    }  
    /*  for(int i=0;i<8;i++){
      for(int j=0;j<8;j++){
	printf("%d ",dist[i][j]);
      }
      printf("\n");
      } */
    //t--;
  }
}

int main(){
    pt a,b;
    //scanf("%d%d%d%d",&a.x,&a.y,&b.x,&b.y);
    while( scanf("%d%d%d%d",&a.x,&a.y,&b.x,&b.y)!=EOF){
      compute(); 
      for(int i=0;i<8;i++){
	for(int j=0;j<8;j++){
	  visited[i][j]=false;
	}
      }
      if(a.x==b.x && a.y==b.y)
	printf("0\n");
      else
	printf("%d\n",BFS(a,b)); 
      //scanf("%d%d%d%d",&a.x,&a.y,&b.x,&b.y);
    }
    return 0;
}
