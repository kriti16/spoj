#include<iostream>
#include<stdio.h>
using namespace std;
int len;
bool num[100005];
int prime[9600];
void create_prime(){
    int k=1,j;
    for(int i=2;i<=100004;i++){
        if(num[i]==false){
            prime[k]=i;k++;
            for(j=2;j*i<=100004;j++)
                num[j*i]=true;
        }
    }
    len=k-1;
    //cout<<len;
}

int main(){
    create_prime();
    prime[0]=1;
    int t;
    cin>>t;
    while(t--){
        int n,k;
        cin>>n>>k;
        if(k==0){
            long long ans=(long long)n*(n-1)/2;
            printf("%lld\n",ans);
        }
        else if(k>len)
            cout<<"0\n";
        else if(prime[k]>n)
            cout<<"0\n";
        else if(prime[k]==n)
            cout<<"1\n";
        else{
            long long ans=0;
            int to=k,from=1;
            while(prime[to]<=n){
                ans+=((prime[from]-prime[from-1])*(n-prime[to]+1));
                to++;
                from++;
            }
            printf("%lld\n",ans);
        }
    }
}
