#include<stdio.h>
int main(){
    int t,n,i;
    scanf("%d",&t);
    while(t--){
        scanf("%d",&n);
        double sum=0;
        for(i=1;i<=n;i++){
            sum+=((double)i)/((double)i*i*i*i+i*i+1);
        }
        printf("%.5lf\n",sum);
    }
}
