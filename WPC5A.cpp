#include<cstdio>
#include<iostream>
#include<math.h>
using namespace std;
typedef long long ll;
bool num[1000000];
int prime[40000];
int len1=0;
void create_prime(){
    int k=0,j;
    for(int i=2;i<1000000;i++){
        if(num[i]==false){
            prime[k]=i;k++;
            for(j=2;j*i<1000000;j++)
                num[j]=true;
        }
    }
    len1=k;
}

int main(){
    create_prime();
    int t;
    scanf("%d",&t);
    while(t--){
        ll n,k;
        int f=0;
        ll total=1;
        scanf("%lld",&n);
        for(int i=1;prime[i]<=sqrt(n+0.001) && i<len1;i++){
            ll temp=n;
            while(temp>0 && temp%prime[i]==0){
                f++;
                temp/=prime[i];
            }
            total*=(2*f+1);
        }
        printf("%lld\n",total/2);
    }
    return 0;
}
