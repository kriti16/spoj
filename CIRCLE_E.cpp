#include<stdio.h>
#include<cmath>
int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        double r1,r2,r3;
        scanf("%lf%lf%lf",&r1,&r2,&r3);
        double ans=r1*r2+r2*r3+r3*r1+2*sqrt(r1+r2+r3)*sqrt(r1*r2)*sqrt(r3);
        ans=r1*r2*r3/ans;
        printf("%.6lf\n",ans);
    }
}
