#include<iostream>
#include<stdio.h>
#include<algorithm>
using namespace std;
int main(){
    int n1,n2;
    cin>>n1;
    while(n1!=0){
        int d1[10001]={0},d2[10001]={0};
        long long sum1=0,sum2=0,sum=0;
        for(int i=0;i<n1;i++)
            cin>>d1[i];
        cin>>n2;
        for(int i=0;i<n2;i++)
            cin>>d2[i];
        int m=min(n1,n2),i;
        for(i=0;i<m;i++){
            sum1+=d1[i];
            sum2+=d2[i];
            if(d1[i]==d2[i]){
                sum+=max(sum1,sum2);
                sum1=0;
                sum2=0;
            }
        }

        if(i==n1 && n1!=n2){
            for(int i=n1;i<n2;i++)
                sum2+=d2[i];
            sum+=max(sum1,sum2);
        }
        else if(i==n2 && n2!=n1){
            for(int i=n2;i<n1;i++)
                sum1+=d1[i];
            sum+=max(sum1,sum2);
        }
        if(n1==n2){
            if(d1[n1-1]!=d2[n2-1])
                sum+=max(sum1,sum2);
        }
        printf("%lld\n",sum);
        cin>>n1;
    }
}
