#include<stdio.h>
#include<cstring>
#include<iostream>
#include<sstream>
using namespace std;

int gcd(int m,int n){
    int a=max(m,n);
    int b=min(m,n);
    int r=a%b;
    while(r!=0){
        a=b;b=r;r=a%b;
    }
    return b;
}

int main(){
    int t,i,n,fact,len;
    scanf("%d",&t);
    while(t--){
        char a[11],c='a';
        scanf("%s",a);
        i=0;
        while(i<strlen(a) && a[i]!='.'){
            i++;
        }
        if(i==strlen(a))
            cout<<"1\n";
        else{
            n=0;fact=1;len=strlen(a)-i-1;
            for(int j=strlen(a)-1;j>i;j--){
                n+=(fact*(a[j]-'0'));
                fact*=10;
            }
            int ans=fact/gcd(fact,n);
            printf("%d\n",ans);
        }

    }
    return 0;
}
