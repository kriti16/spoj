#include<cstdio>
#include<iostream>
#include<vector>
#include<string.h>
using namespace std;
typedef long long ll;
vector<ll> prime;
bool is_prime[40000000];
void create_prime(){
    memset(is_prime,true,40000000);
	is_prime[0] = is_prime[1] = false;
	for(ll i=2; i<40000000; i++){
		if(is_prime[i]){
			for(ll j = i*i; j<40000000; j=j+i){
				is_prime[j] = false;
			}
			prime.push_back(i);
		}
	}
}

int main(){
    create_prime();
    ll n;
    scanf("%lld",&n);
    while(n!=0){
        ll temp=n;
        for(ll i=0;prime[i]*prime[i]<=n && i<prime.size();i++){
            ll f=0;
            while(temp>0 && temp%prime[i]==0){
                f++;
                temp/=prime[i];
            }
            if(f>0)
                printf("%lld\^%lld ",prime[i],f);
            if(temp==1)
                break;
        }
        if(temp>1)
                printf("%lld\^1",temp);
        printf("\n");
        scanf("%lld",&n);
    }
    return 0;
}
