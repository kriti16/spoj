#include<iostream>
#include<stdio.h>
using namespace std;

int gcd(int m,int n){
    int a=max(m,n);
    int b=min(m,n);
    int r=a%b;
    while(r!=0){
        a=b;b=r;r=a%b;
    }
    return b;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int a,b;
        scanf("%d %d",&a,&b);
        int r=gcd(a,b);
        printf("%d %d\n",b/r,a/r);
    }
}
