#include<cstdio>
#include<algorithm>
using namespace std;

const int M=5000000;

struct node{
    int value;
    int index;
};

int n,k,mat[51][10005],s[10005];
node temp[10005];

bool compare(node lhs,node rhs){
    return lhs.value<rhs.value;
}




void fill_mat(){
    for(int len=1;len<=k;len++)
            fill_row(len,0,n-1);
}

int main(){
    while(1){
    scanf("%d %d",&n,&k);
    for(int i=0;i<n;i++)
        scanf("%d",&s[i]);
    fill_mat();
    int sum=0;
    for(int i=0;i<n;i++)
        sum+=(mat[k][i]%M);
    printf("%d\n",sum);
    }
    return 0;
}
