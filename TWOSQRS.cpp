#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<cmath>
#define gc getchar_unlocked
#define pc(x) putchar_unlocked(x);
using namespace std;
typedef long long ll;
ll sq[1000005];
void compute(){
    for(ll i=0;i<=1000003;i++){
        sq[i]=i*i;
    }
}

int main(){
    compute();
    int t;
    scanf("%d",&t);
    while(t--){
        ll n;
        scanf("%lld",&n);
        ll i;
        ll m=(ll)sqrt(n);
        //cout<<m<<endl;
        for(i=0;i<=m;i++){
            if(binary_search(sq,sq+m+2,n-sq[i])){
                break;
            }
            //cout<<"i:"<<sq[i]<<endl;
        }
        if(i>m)
            printf("No\n");
        else
            printf("Yes\n");
    }
}
