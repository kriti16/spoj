#include<stdio.h>
#include<algorithm>
#include<iostream>
using namespace std;
typedef unsigned long long ull;
struct node{
    int h;
    int c;
    int csum;
};
node a[10002];
bool myfunction (node i,node j) { return (i.h<j.h); }
int n;
ull fun(int x){
    ull sum=0;
    for(int i=1;i<x;i++)
        sum+=((a[x].h-a[i].h)*a[i].c);
    for(int i=x+1;i<=n;i++)
        sum+=((a[i].h-a[x].h)*a[i].c);
    return sum;
}

int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        int total=0;
        scanf("%d",&n);
        for(int i=1;i<=n;i++)
            scanf("%d",&a[i].h);
        for(int i=1;i<=n;i++){
            scanf("%d",&a[i].c);
        }
        sort(a+1,a+n+1,myfunction);
        for(int i=1;i<=n;i++){
            total+=a[i].c;
            a[i].csum=total;
            //cout<<a[i].csum<<endl;
        }

        ull low=fun(1);
        int i=1;
        //cout<<low<<endl;
        while(i<n){
            while(total-2*a[i].csum >=0 && i<n)
                i++;
            //printf("%d %llu\n",i,low);
            ull temp=fun(i);
            if(temp<low)
                low=temp;
            while(total-2*a[i].csum <0 && i<n)
                i++;
        }
        low=min(fun(n),low);
        printf("%llu\n",low);
    }
    return 0;
}
