#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
#include<math.h>
#include<set>
#include<vector>
#include<cstring>
#include<string>
#include<map>
#include<climits>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
int main(){
    int t;
    scanf("%d",&t);
    for(int x=1;x<=t;x++){
        int n,num;
        scanf("%d",&n);
        ll sum=0,req=1;
        int flag=0;
        for(int i=0;i<n;i++){
            scanf("%d",&num);
            sum+=num;
            if(sum<=0){
                if(flag==0){
                    req+=-sum;
                    flag=1;
                }
                else
                    req+=(1-sum);
                sum=1;
            }
        }
        printf("Scenario #%d: %lld\n",x,req);
    }
    return 0;
}

