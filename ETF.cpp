#include<cstdio>
using namespace std;
#define p 1000000
int t[1000001];

void compute(){
    for(int i=1;i<=p;i++){
        t[i]=i;
    }

    for(int i=2;i<=p;i++){
        if(t[i]==i){
            for(int k=1;i*k<=p;k++){
                t[i*k]=t[i*k]/i*(i-1);
            }
        }
    }

}

int main(){
    int f;
    scanf("%d",&f);
    compute();
    while(f--){
        int n;
        scanf("%d",&n);
        printf("%d\n",t[n]);
    }
    return 0;
}

