#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
const int M=5000000;

struct node{
    int value;
    int index;
};

int n,k,mat[51][10005],s[10005];
node temp[10005];

bool compare(node lhs,node rhs){
    return lhs.value<rhs.value;
}

void fill_row(int len,int left,int right){
    int mid=(left+right)/2;
    if(left<right){
        fill_row(len,left,mid);
        for(int p=left;p<=right;p++){
                temp[p].value=s[p];
                temp[p].index=p;
        }
        sort(temp+left,temp+mid+1,&compare);
        sort(temp+mid+1,temp+right+1,&compare);
        int i=left,j=mid+1;
        while(i<=mid && j<=right){
            if(temp[i].value<temp[j].value){
                    mat[len][temp[j].index]+=mat[len-1][temp[i].index];
                    i++;
            }
            else
                j++;
        }
        j++;
        while(j<=right){
            mat[len][temp[j].index]=mat[len][temp[j-1].index];
            j++;
        }
        printf("len:%d\n",len);
        for(int i=0;i<n;i++)
            printf("%d ",mat[len][i]);
        printf("\n");

    }
}

void fill_mat(){
    for(int len=2;len<=k;len++)
            fill_row(len,0,n-1);
}

int main()
{
    while(1){
    scanf("%d %d",&n,&k);
    for(int i=0;i<n;i++){
        scanf("%d",&s[i]);
        for(int j=0;j<=k;j++)
            mat[j][i]=0;
    }
    for(int i=0;i<n;i++)
        mat[1][i]=1;

    fill_mat();
    int sum=0;
    for(int i=0;i<n;i++)
        sum+=(mat[k][i]%M);

    printf("%d\n",sum);
    }
    return 0;
}
