#include<iostream>
#include<stdio.h>
using namespace std;
#define M 1000007
typedef long long ll;
int main(){
    int t;
    cin>>t;
    while(t--){
        ll n;
        scanf("%lld",&n);
        ll ans=(3*n*n%M+n)%M*500004%M;
        printf("%lld\n",ans);
    }
}
