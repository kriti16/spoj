#include<iostream>
#include<cstdio>
#include<set>
#include<stack>
#include<string>
#include<algorithm>
using namespace std;
ostream & operator<<(ostream & os, set<string> f) {
    set<string>::iterator sti;
    for(sti = f.begin(); sti != f.end(); sti++)
        os << *sti;
    return os;
}

int main(){
    int t;
    scanf("%d",&t);
    stack< set<string> > s;
    while(t--){
        int n;
        scanf("%d",&n);
        string cmd;
        while(n--){
            cin>>cmd;
            if(cmd=="PUSH"){
                set<string> a;
                s.push(a);
                printf("%d\n",(s.top()).size());
               // cout<<s.top()<<endl;
            }

            if(cmd=="DUP"){
                set<string> a=s.top();
                s.push(a);
                printf("%d\n",(s.top()).size());
                //cout<<s.top()<<endl;
            }

            if(cmd=="UNION"){
                set<string> a=s.top();
                s.pop();
                set<string> b=s.top();
                s.pop();
                set<string> c;
                set_union(a.begin(),a.end(),b.begin(),b.end(),insert_iterator<set<string> >(c,c.begin()));
                s.push(c);
                printf("%d\n",(s.top()).size());
                //cout<<s.top()<<endl;
            }

            if(cmd=="INTERSECT"){
                set<string> a=s.top();
                s.pop();
                set<string> b=s.top();
                s.pop();
                set<string> c;
                set_intersection(a.begin(),a.end(),b.begin(),b.end(),insert_iterator<set<string> >(c,c.begin()));
                s.push(c);
                printf("%d\n",(s.top()).size());
            }

            if(cmd=="ADD"){
                set<string> a=s.top();
                s.pop();
                string b="{";
                if(a.begin()==a.end())
                    b="{}";
                else{
                    for(set<string>::iterator it=a.begin();it!=a.end();it++){
                        b.append(*it);
                        b.append(",");
                    }
                    b.replace(b.end()-1,b.end(),"}");
                }
                s.top().insert(b);
                printf("%d\n",(s.top()).size());
               // cout<<s.top()<<endl;
            }
        }
        while(!s.empty())
            s.pop();
        printf("***\n");
    }
    return 0;
}
