#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
#include<math.h>
#include<set>
#include<vector>
#include<cstring>
#include<string>
#include<limits>
#include<map>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
ll s[100005],a[100005];
int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        int n;
        scanf("%d",&n);
        for(int i=0;i<n;i++)
            scanf("%lld",&a[i]);
        s[0]=a[0];
        //cout<<s[0];
        for(int i=1;i<n;i++){
            if(s[i-1]>0)
                s[i]=s[i-1]+a[i];
            else
                s[i]=a[i];
           // cout<<" "<<s[i];
        }
        //printf("\n");
        ll m=s[0];
        ll f=1,co=0;
        for(int i=1;i<n;i++){
            if(s[i-1]==0)
                co++;
            if(s[i]>m){
                m=s[i];
                f=1;
                if(s[i-1]==0)
                    f+=co;
                //cout<<f;
            }
            else if(s[i]==m){
                f++;
                if(s[i-1]==0)
                    f+=co;
                //cout<<f<<endl;
            }
            //cout<<"s[i]:"<<s[i]<<" co:"<<co<<" f:"<<f<<" m:"<<m<<endl;
        }
        printf("%d %d\n",m,f);
    }
    return 0;
}

