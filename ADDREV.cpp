#include<cstdio>
#include<math.h>
#include<cstring>
using namespace std;
typedef long long ll;
int main(){
    int n;
    scanf("%d",&n);
    while(n--){
        ll n1=0,n2=0,n3=0;
        char c1[20],c2[20],d1[20],d2[20],ans[20];
        scanf("%s %s",c1,c2);
        for(int i=strlen(c1)-1;i>=0;i--){
                //printf("%c %f\n",c1[i],(float)pow(10,i));
            n1+=((ll)((c1[i]-'0')*(pow(10,i))+0.001));
            //d1[strlen(c1)-1-i]=c1[i];
            //printf("%lld\n",n1);
        }
        //printf("%s\n%lld\n",c1,n1);
        for(int i=strlen(c2)-1;i>=0;i--){
            n2+=((ll)((c2[i]-'0')*(pow(10,i))+0.001));
            //d2[strlen(c1)-1-i]=c2[i];
            //printf("%lld\n",n2);
        }
        //printf("%s\n%lld\n",c2,n2);
        n3=n1+n2;
        int j=0; int flag=0;
        while(n3!=0){
            if(flag==0 && n3%10==0)n3/=10;
            else{
                ans[j]=n3%10+'0';j++;
                n3/=10;
                flag=1;
            }
        }
        ans[j]='\0';
        printf("%s\n",ans);
    }
}
