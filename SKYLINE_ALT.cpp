#include<iostream>
#include<stdio.h>
using namespace std;
#define M 1000000
long long peri(int);
long long perd(int);
int N;
long long C(int n,int r){
    if(r==0)
        return 1;
    return n*C(n-1,r-1)/r%M;
}

long long peri(int n){
    if(n==0)
        return 1;
    if(n==1)
        return 0;
    if(n==2)
        return 1;
    long long ans=0;
    for(int i=0;i<n;i++){
        if(i==0)
            ans+=((C(n-1,i)*peri(n-i-1))%M);
        else
            ans+=(((C(n-1,i)*peri(n-i-1))%M)*perd(i)%M);
        cout<<"n: "<<n<<" peri "<<"i: "<<i<<" ans: "<<ans<<endl;
    }
    cout<<"PERI OF "<<n<<" IS "<<ans<<endl;
    return ans;
}

long long perd(int n){
    if(n==0)
        return 0;
    if(n==1)
        return 1;
    if(n==2)
        return 1;
    long long ans=0;
    for(int i=0;i<n;i++){
        if(i==0)
            ans+=((C(n-1,i)*perd(n-i-1))%M);
        else
            ans+=(((C(n-1,i)*perd(n-i-1))%M)*perd(i)%M);
        cout<<"n: "<<n<<" perd "<<"i: "<<i<<" ans: "<<ans<<endl;
    }
    cout<<"PERD OF "<<n<<" IS "<<ans<<endl;
    return ans;
}

int main(){
    int t,n;
    cin>>t;
    while(t--){
        cin>>n;
        int ans=(peri(n)+perd(n))%M;
        cout<<ans<<endl;
    }
}
