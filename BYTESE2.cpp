#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
#include<math.h>
#include<set>
#include<vector>
#include<cstring>
#include<string>
#include<map>
#include<climits>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
struct node{
    int s;
    int f;
};
node ti[110];

int compare (const void * a, const void * b){

  node *orderA = (node *)a;
  node *orderB = (node *)b;

  return ( -orderB->s + orderA->s );
}

int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        int n;
        scanf("%d",&n);
        for(int i=0;i<n;i++){
            scanf("%d %d",&ti[i].s,&ti[i].f);
        }
        qsort(ti,n,sizeof(node),compare);
     /*   for(int i=0;i<n;i++){
            printf("%d %d",time[i].s,time[i].f);
        }  */
        int m=0,ml;
        for(int i=0;i<n;i++){
            ml=1;
            for(int j=0;j<i;j++){
                if(ti[j].f>ti[i].s)
                    ml++;
            }
            if(ml>m)
                m=ml;
        }
        printf("%d\n",m);
    }
    return 0;
}

