#include<iostream>
#include<stdio.h>
#include<cmath>
using namespace std;
int main(){
    int n;
    cin>>n;
    while(n!=-1){
        if((n-1)%3!=0){
            cout<<"N\n";
        }
        else{
            n=(n-1)/3;
            int s=(int)sqrt(n);
            if(s*(s+1)==n)
                cout<<"Y\n";
            else
                cout<<"N\n";
        }
        cin>>n;
    }
    return 0;
}
