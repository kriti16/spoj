#include<stdio.h>
#include<cmath>
typedef long long ll;
using namespace std;
int main(){
    int t;
    scanf("%d",&t);
    for(int i=1;i<=t;i++){
        ll n;
        scanf("%lld",&n);
        ll ans=(ll)sqrt(n+1+0.0001)-1;
        printf("Case %d: %lld\n",i,ans);
    }
}
