#include<iostream>
#include<stdio.h>
#include<algorithm>
using namespace std;
bool num[1000001];
int prime[80000];
int len;
void create_prime(){
    int k=0,j;
    for(int i=1;i<=1000000;i++)
        num[i]=true;
    for(int i=2;i<=1000000;i++){
        if(num[i]==true){
            prime[k]=i;k++;
            for(j=2;j*i<=1000000;j++)
                num[j*i]=false;
        }
    }
    //cout<<k<<endl;
    len=k;
}
bool check[1000000];
int dp[7000],total_dp,ans[1000006];
void compute(){
    for(int i=1;i<=1000000;i++)
        check[i]=true;
    for(int i=1;i<=1000000;i++){
        int temp=i,num=i;
        while(temp){
            num+=(temp%10);
            temp/=10;
        }
        check[num]=false;
    }
    int k=0;
    for(int i=0;i<len;i++){
        if(check[prime[i]]){
            dp[k]=prime[i];
            k++;
        }
    }
    total_dp=k;
    //cout<<total_dp<<" dp\n";
    // for(int i=0;i<20;i++)
    //cout<<dp[0]<<" dp[0]\n";
   for(int i=0;i<dp[0];i++)
        ans[i]=0;
   for(int i=1;i<total_dp;i++){
        for(int j=dp[i-1];j<dp[i];j++)
            ans[j]=i;
   }
   for(int i=dp[total_dp-1];i<=1000000;i++)
        ans[i]=total_dp;
  // for(int i=0;i<20;i++)
  //      cout<<ans[i]<<" ";
}
int main(){
    create_prime();
    compute();
    int q;
    scanf("%d",&q);
    while(q--){
        int a,b;
        scanf("%d%d",&a,&b);
        if(a==0)
            printf("%d\n",ans[b]);
        else
            printf("%d\n",ans[b]-ans[a-1]);
    }
    return 0;
}
