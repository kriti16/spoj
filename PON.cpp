#include<cstdio>
#include<stdlib.h>
using namespace std;
typedef unsigned long long ull;

ull mulmod(ull a,ull b,ull c){
    ull x = 0,y=a%c;
    while(b > 0){
        if(b%2 == 1){
            x = (x+y)%c;
        }
        y = (y*2)%c;
        b /= 2;
    }
    return x%c;
}

ull power(ull a,ull p,ull n){
    if(p==0)return 1;
    if(p==1)return a%n;
    ull temp=power(a,p/2,n);
    //printf("%llu\n",temp);
    ull ans=mulmod(temp,temp,n);
    if(p%2==0)return ans;
    else return mulmod(ans,a,n);
}

bool mr_test(ull n){
   if(n<2)return false;
   if(n==2 || n==3)return true;
   if(n%2==0)return false;
   int p=0;
   ull s=n-1;
   while(s%2==0){
     p++;
     s/=2;
   }
   ull a=rand()%(n-3)+2;
   if(power(a,s,n)==1)
        return true;
   for(int j=0;j<p;j++){
        ull k=1<<j;
        //printf("%llu\n",k);
        if(power(a,s*k,n)==n-1){
            return true;
        }
   }
   return false;
}

int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        ull n;
        scanf("%llu",&n);
        if(mr_test(n))
            printf("YES\n");
        else
            printf("NO\n");
    }
    return 0;
}

