#include<stdio.h>
#include<math.h>
typedef long long ll;
ll determinant(int a[15][15],int k)
{
  int s=1,b[15][15];
  ll det=0;
  int i,j,m,n,c;
  if (k==1)
     return (a[1][1]);
  else{
     det=0;
     for(c=1;c<=k;c++){
        m=0;
        n=0;
        for(i=1;i<=k;i++){
            for (j=1;j<=k;j++){
                b[i][j]=0;
                if (i != 0 && j != c){
                   b[m][n]=a[i][j];
                   if (n<=(k-2))
                    n++;
                   else{
                     n=0;
                     m++;
                    }
                 }
               }
              }
          det=det + s * (a[0][c] * determinant(b,k-1));
          s=-1 * s;
          }
      }
      return det;
}

void transpose(int num[15][15],int fac[15][15],int r)
{
  int i,j;
  int b[15][15],inverse[15][15],d;
 
  for (i=1;i<=r;i++)
    {
     for (j=1;j<=r;j++)
       {
         b[i][j]=fac[j][i];
        }
    }
  d=determinant(num,r);
  for (i=1;i<=r;i++)
    {
     for (j=1;j<=r;j++)
       {
        inverse[i][j]=b[i][j] / d;
        }
    }
   printf("\n\n\nThe inverse of matrix is : \n");
 
   for (i=1;i<=r;i++)
    {
     for (j=1;j<=r;j++)
       {
         printf("\t%f",inverse[i][j]);
        }
    printf("\n");
     }
}


void cofactor(int num[15][15],int f)
{
 int b[15][15],fac[15][15];
 int p,q,m,n,i,j;
 for (q=1;q<=f;q++){
   for (p=1;p<=f;p++){
     m=0;
     n=0;
     for (i=1;i<=f;i++){
       for (j=1;j<=f;j++){
          if (i != q && j != p){
            b[m][n]=num[i][j];
            if (n<=(f-2))
             n++;
            else{
               n=0;
               m++;
             }
           }
        }
      }
      fac[q][p]=pow(-1,q + p) * determinant(b,f-1);
    }
  }
  transpose(num,fac,f);
}



int main()
{
  int t;
  scanf("%d",&t);
  while(t--){
	  int n,r,k,val[15],a[15][15];
	  scanf("%d%d%d",&n,&r,&k);
	  for(int i=1;i<=r;i++)
		for(int j=1;j<=r;j++)
			a[i][j]=0;
	  for(int i=1;i<=r;i++)
		scanf("%d",&val[i]);
	  for(int i=1;i<=r;i++)
		scanf("%d",&a[1][i]);
	  a[1][1]--;
	  for(int i=2;i<=r;i++){
		a[i][i-1]=1;
		a[i][i]--;
	  }
	  ll d=determinant(a,r);
	  printf("Determinant of the Matrix = %lld",d);
	  cofactor(a,r);
  }
}
 


