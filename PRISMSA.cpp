#include<stdio.h>
#include<cmath>
using namespace std;
int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        int v;
        scanf("%d",&v);
        double a=cbrt(4.0*v);
        double ans=6.0*v*sqrt(3)/a;
        printf("%.10lf\n",ans);
    }
    return 0;
}
