#include<stdio.h>
#include<ctime>
int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        int g,m,num;
        scanf("%d%d",&g,&m);
        int m1=0,m2=0;
        for(int i=0;i<g;i++){
            scanf("%d",&num);
            if(m1<num)
                m1=num;
        }
        for(int i=0;i<m;i++){
            scanf("%d",&num);
            if(m2<num)
                m2=num;
        }
        if(m1>=m2)
            printf("Godzilla\n");
        else
            printf("MechaGodzilla\n");
    }
}
