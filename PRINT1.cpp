#include<stdio.h>
#include<math.h>
#include<ctime>
#include<stdlib.h>
#include<iostream>
#define gc getchar_unlocked
#define pc(x) putchar_unlocked(x);
using namespace std;
typedef unsigned long long int ull;
ull len;
ull prime[5000];
bool num[46400];
inline void writeInt (ull n){
    ull N = n, rev, count = 0;
    rev = N;
    if (N == 0) { pc('0'); pc('\n'); return ;}
    while ((rev % 10) == 0) { count++; rev /= 10;} //obtain the count of the number of 0s
        rev = 0;
    while (N != 0) { rev = (rev<<3) + (rev<<1) + N % 10; N /= 10;}  //store reverse of N in rev
    while (rev != 0) { pc(rev % 10 + '0'); rev /= 10;}
    while (count--) pc('0');
}

void scanint(ull &x){
    register ull c = gc();
    x = 0;
    for(;(c<48 || c>57);c = gc());
    for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
}

void print(string p){
    int ln=p.length();
    for(int i=0;i<ln;i++)
        putchar_unlocked(p[i]);
}

void create_prime(){
    int k=0,j;
    for(int i=2;i<=46400;i++){
        if(num[i]==false){
            prime[k]=i;
            k++;
            for(j=2;j*i<=46400;j++)
                num[j*i]=true;
        }
    }
    len=k;
    cout<<len<<endl;
}

int main(){
    create_prime();
    int t;
    scanf("%d",&t);
    ull m,n;
    while(t--){
        scanint(m);
        scanint(n);
        bool check[1000000];
        for(ull i=0;i<=(n-m);i++)
            check[i]=true;
        ull limit=(ull)sqrt(n)+1;
        for(ull i=0;prime[i]<=limit;i++){
            for(ull j=m/prime[i]*prime[i];j<=n;j+=prime[i]){
                if(j-m>0)
                    check[j-m]=false;
            }
        }

        for(int i=0;i<=(n-m);i++){
            if(check[i]){
                writeInt(i+m);
                print("\n");
            }
        }
    }
    return 0;
}
