#include<stdio.h>
#include<math.h>
#include<ctime>
#include<stdlib.h>
#include<iostream>
#define gc getchar_unlocked
#define pc(x) putchar_unlocked(x);
using namespace std;
typedef unsigned long long int ull;

inline void writeInt (ull n){
    ull N = n, rev, count = 0;
    rev = N;
    if (N == 0) { pc('0'); pc('\n'); return ;}
    while ((rev % 10) == 0) { count++; rev /= 10;} //obtain the count of the number of 0s
        rev = 0;
    while (N != 0) { rev = (rev<<3) + (rev<<1) + N % 10; N /= 10;}  //store reverse of N in rev
    while (rev != 0) { pc(rev % 10 + '0'); rev /= 10;}
    while (count--) pc('0');
}

void scanint(ull &x){
    register ull c = gc();
    x = 0;
    for(;(c<48 || c>57);c = gc());
    for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
}

ull pow(ull a,ull d,ull n){
    if(d==0)
        return 1;
    ull temp=pow(a,d/2,n);
    //cout<<"temp:"<<temp<<endl;
    if(d%2)
        return temp*temp%n*a%n;
    else
        return temp*temp%n;
}

int main(){
    int t;
    scanf("%d",&t);
    ull m,n;
    while(t--){
        scanint(m);
        scanint(n);
        for(ull i=m;i<=n;i++){
            if(i==1)
                continue;
            if(i==2 || i==3){
                writeInt(i);
                printf("\n");
                continue;
            }
            if(i%2==0)
                continue;
            ull temp=i-1,s=0,d;
            while(temp%2==0){
                temp/=2;
                s++;
            }
            d=(i-1)/(1<<s);
            int times=2;
            ull a,prev;
            while(times--){
                int flag=0;
                srand(time(0));
                a=rand()%(i-3)+2;
                while(a==prev)
                    a=rand()%(i-3)+2;
                prev=a;
                ull x=pow(a,d,i);
                if(x==1 || x==n-1){
                    continue;
                }
                else{
                    for(ull i=1;i<s;i++){
                    x=(x*x)%n;
                    if(x==n-1){
                        continue;
                    }
                    if(x==1){
                        flag=1;
                        break;
                    }
                }
            }
            if(!flag)

        }
    }
    return 0;
}
