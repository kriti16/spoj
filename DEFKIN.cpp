#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
#include<math.h>
#include<set>
#include<vector>
#include<cstring>
#include<string>
#include<map>
#include<climits>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
int x[40005],y[40005];
int main(){
    int t;
    scanf("%d",&t);
    while(t--){
        int w,h,n;
        scanf("%d%d%d",&w,&h,&n);
        x[0]=0,y[0]=0;
        for(int i=1;i<=n;i++){
            scanf("%d%d",&x[i],&y[i]);
        }
        sort(x,x+n+1);
        sort(y,y+n+1);
        ll mdx=0,mdy=0;
        x[n+1]=w+1;
        y[n+1]=h+1;
        for(int i=0;i<=n;i++){
            if(x[i+1]-x[i]-1>mdx)
                mdx=x[i+1]-x[i]-1;
        }
        for(int i=0;i<=n;i++){
            if(y[i+1]-y[i]-1>mdy)
                mdy=y[i+1]-y[i]-1;
        }
        ll ans=mdx*mdy;
        printf("%lld\n",ans);
    }
    return 0;
}

