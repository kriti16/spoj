#include<iostream>
#include<stdio.h>
#include<algorithm>
using namespace std;
int main(){
    int t,j,k=1,m,n,s[1001];
    cin>>t;
    j=t;
    while(t--){
        cin>>m>>n;
        for(int i=0;i<n;i++){
            cin>>s[i];
        }
        printf("Scenario #%d:\n",k);k++;
        sort(s+0,s+n);

        long long sum=0;
        int i=n-1,f=0;
        while(sum<m && i>=0){
            sum+=s[i];
            i--;
            f++;
        }
        if(sum<m)
            cout<<"impossible\n\n";
        else
            cout<<f<<"\n\n";
    }
}
