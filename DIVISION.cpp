#include<iostream>
#include<stdio.h>
using namespace std;
typedef long long ll;
#define P 1000000007

ll pow(int n,ll p){
    if(p==0)
        return 1;
    ll temp=pow(n,p/2)%P;
    if(p%2)
        return temp*temp%P*n%P;
    else
        return temp*temp%P;
}

int main(){
    ll n;
    while(scanf("%lld",&n)!=EOF){
    ll p=n%(P-1);
    //cout<<p<<endl;
    ll ans;
    if(p%2){
        ans=((pow(2,p)-2)*333333336+1)%P;
    }
    else
        ans=((pow(2,p)-1)*333333336+1)%P;
    printf("%lld\n",ans);
    }
}

